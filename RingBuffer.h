#ifndef _RING_BUFFER_H
#define _RING_BUFFER_H

#include <stdio.h>
#include <stdint.h>

#define RING_BUFFER_SIZE	256

typedef struct
{
  	uint8_t* buffer;
	uint16_t idxIn;
	uint16_t idxOut;
	uint16_t size;
} RingBuffer_t;

uint8_t rBuffer_init(RingBuffer_t* rBuffer, uint16_t size);
void rBuffer_put(RingBuffer_t* rBuffer, uint8_t data);
void rBuffer_putString(RingBuffer_t* rBuffer, uint8_t* data);
void rBuffer_putData(RingBuffer_t* rBuffer, uint8_t* data, uint16_t dataCount);
uint8_t rBuffer_pop(RingBuffer_t* rBuffer);
uint16_t rBuffer_getCount(RingBuffer_t* rBuffer);
uint8_t rBuffer_showByte(RingBuffer_t* rBuffer, uint16_t position);
void rBuffer_clear(RingBuffer_t* rBuffer);

#endif //_RING_BUFFER_H
