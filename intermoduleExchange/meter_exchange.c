#include "meter_exchange.h"

#include <string.h>

#

#define RECIVE_MESSAGE_SIZE_POSITION        (7)

static GPRSSetup        gprsSetup;
static AutoConnect      autoConnect;
static PushSetupDest    pushSetupDest;

static bool needUpdateFlag = false;

const uint8_t configPreamble[4] = {0xAA, 0xBB, 0xCC, 0xDD};

enum
{
    MESSAGE_TYPE_COMMAND        = 0,
    MESSAGE_TYPE_CONFIGURATION  = 1
};

typedef enum
{
    COMMAND_TYPE_PUSH_MESSAGE           = 0,
} CommandType;


typedef enum
{
    CFG_PARAMETERS_GPRS_SETUP           = 0,
    CFG_PARAMETERS_AUTO_CONNECT_SETUP   = 1,
    CFG_PARAMETERS_PUSH_SETUP_DEST      = 2,

    CFG_PARAMETER_COUNT
} CfgParameters;

bool CONFIG_isConfigurationMessage(RingBuffer_t *buffer)
{
    // get first 4 bytes for checking
    for (uint8_t i = 0; i < sizeof(configPreamble); i++)
    {
        if (configPreamble[i] != rBuffer_showByte(buffer, i))
        {
            return false;
        }
    }

    return true;
}

typedef struct HeaderStruct 
{
    uint8_t preamble[4];
    uint8_t type;
    uint8_t id;
    uint8_t size;
} Header;

static inline void fillHeader(uint8_t *header, RingBuffer_t *buffer)
{
    for (uint8_t i = 0; i < sizeof(Header); i++)
    {
        header[i] = rBuffer_showByte(buffer, i);
    }
}

const uint8_t objectSizes[CFG_PARAMETER_COUNT] = 
{
    sizeof(gprsSetup),
    sizeof(autoConnect),
    sizeof(pushSetupDest)
};

const uint32_t* objectPtr[CFG_PARAMETER_COUNT] =
{
    (uint32_t*)&gprsSetup,
    (uint32_t*)&autoConnect,
    (uint32_t*)&pushSetupDest
};

static void (*fillPushMessage)(const uint8_t*, uint8_t) = NULL;

void METEREXCHANGE_subscribeFillPushMessageCallback(void (*callback)(const uint8_t*, uint8_t))
{
    fillPushMessage = callback;
}

#define HEADER_SIZE     (sizeof(Header) + 2) // 2 - sizeof(crc)

static inline void handlePushSetupMessage(Header *header, const uint8_t* message)
{
    if (fillPushMessage == NULL)
    {
        return;
    }

    uint8_t payloadSize = header->size - HEADER_SIZE;

    fillPushMessage(message, payloadSize);
}

static inline void handleCommand(Header *header, const void *data)
{
    if (header->id == COMMAND_TYPE_PUSH_MESSAGE)
    {
        handlePushSetupMessage(header, data);
    }
}

static inline bool handleConfiguration(Header *header, const void *data)
{
#define SIZE_OF_CRC     (2)
    uint8_t payloadSize = header->size - HEADER_SIZE;
    if ((header->id >= CFG_PARAMETER_COUNT) ||
        (payloadSize != objectSizes[header->id]))
    {
        return false;
    }

    memcpy((void*)objectPtr[header->id], data, header->size);
    needUpdateFlag = true;

    return true;
}

static inline uint16_t calcCRC16(uint8_t *pbyte, uint16_t lengh)  //f(x) = x16 +x12+x5+1
{
    uint16_t crc = 0xFFFF;
    while (lengh--)
    {
        uint8_t x = crc >> 8 ^ *pbyte++;
        x ^= x >> 4;
        crc = (crc << 8) ^ ((uint16_t) (x << 12)) ^ ((uint16_t) (x << 5)) ^ ((uint16_t)x);
    }

    return crc;
}

bool METEREXCHANGE_handle(RingBuffer_t *buffer)
{
    if (!CONFIG_isConfigurationMessage(buffer))
    {
        return false;
    }

    uint8_t tmpBuffer[100]; // max length

    Header header;
    fillHeader((uint8_t*)&header, buffer);

    if (header.size > rBuffer_getCount(buffer))
    {
        return true; // FIXME
    }

    for (uint8_t i = 0; i < header.size; i++)
    {
        tmpBuffer[i] = rBuffer_pop(buffer);
    }

    uint16_t crc = (tmpBuffer[header.size - 1] << 8) | tmpBuffer[header.size - 2];
    uint16_t calcCRC = calcCRC16(tmpBuffer, header.size - 2);

    if (crc != calcCRC)
    {
        return true; // FIXME
        // TODO: add error response
    }

    if (header.type == MESSAGE_TYPE_COMMAND)
    {
        handleCommand(&header, tmpBuffer + sizeof(header));
    }
    else
    {
        handleConfiguration(&header, tmpBuffer + sizeof(header));
    }

    return true;
}

void METEREXCHANGE_getGPRSSetup(GPRSSetupLink gprs)
{
    memcpy(gprs, &gprsSetup, sizeof(gprsSetup));
}

void METEREXCHANGE_getAutoConnect(AutoConnectLink connect)
{
    memcpy(connect, &autoConnect, sizeof(autoConnect));
}

void METEREXCHANGE_getPushDestination(PushSetupDestLink dst)
{
    memcpy(dst, &pushSetupDest, sizeof(pushSetupDest));
}

bool METEREXCHANGE_needUpdate(void)
{
    bool tmp = needUpdateFlag;
    needUpdateFlag = false;

    return tmp;
}
