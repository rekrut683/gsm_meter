#ifndef __METER_EXCHANGE_H__
#define __METER_EXCHANGE_H__

#include <stdint.h>
#include <stdbool.h>

#include "RingBuffer.h"

#define NET_ADDR_STRING_SIZE        (20)

typedef enum 
{
    AUTO_CONNECT_MODE_101 = 101,  // Постоянное подключение к сети
    AUTO_CONNECT_MODE_102 = 102,  // Постоянное подключение в течении установленных окон(не передаются на модем) 
    AUTO_CONNECT_MODE_103 = 103,  // Возможности 102 режима с дополнительной опцией подключения по команде "connect".
    AUTO_CONNECT_MODE_104 = 104,  // Постоянно отключено, подключение к сети только по вызову метода "connect"
} AutoConnectMode;

#pragma pack(push,1)
struct GPRSSetupStruct
{
    uint8_t  apn[29];
    uint8_t  sizeAPN;
    uint16_t pinCode;
};
typedef struct GPRSSetupStruct GPRSSetup, *GPRSSetupLink;

typedef uint8_t Destination[NET_ADDR_STRING_SIZE];

struct AutoConnectStruct
{
    AutoConnectMode mode;
    uint8_t         repetitionCount;
    uint16_t        repetitionDelay;      // in ms

    Destination destination[4];
    uint8_t count;
};
typedef struct AutoConnectStruct AutoConnect, *AutoConnectLink;

struct PushSetupDestStruct
{
    uint8_t address[NET_ADDR_STRING_SIZE];
    uint8_t size;
};
typedef struct PushSetupDestStruct PushSetupDest, *PushSetupDestLink;
#pragma pack(pop)

bool METEREXCHANGE_handle(RingBuffer_t *buffer);

/**
  * @brief Returns a flag indicating the need to update the parameters. After receiving the flag state, it is reset
  * @return Flag state;
*/
bool METEREXCHANGE_needUpdate(void);

/**
  * @brief Update parameters GPRS setup
  * @param [out] gprs - pointer to struct for write parameters GPRS setup
*/
void METEREXCHANGE_getGPRSSetup(GPRSSetupLink gprs);

/**
  * @brief Update parameters auto connect
  * @param [out] connect - pointer to struct for write parameters Auto connect
*/
void METEREXCHANGE_getAutoConnect(AutoConnectLink connect);

/**
  * @brief Update parameters push setup destination
  * @param [out] dst - pointer to struct for write parameters push setup destination
*/
void METEREXCHANGE_getPushDestination(PushSetupDestLink dst);

/**
  * @brief Subscribes to a method that fills the push message ring buffer
  * @param [in] callback - pointer to callback function for  ring buffer fill
*/
void METEREXCHANGE_subscribeFillPushMessageCallback(void (*callback)(const uint8_t*, uint8_t));

#endif // __METER_EXCHANGE_H__
