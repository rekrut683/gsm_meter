#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <stdint.h>

void DEBUG_init(uint16_t size);
void DEBUG_sendString(uint8_t *string);
void DEBUG_sendData(uint8_t *data, uint16_t size);

uint16_t DEBUG_getDataSize(void);
uint8_t DEBUG_popByte(void);

#endif // __DEBUG_H__
