#ifndef __ASSISTANT_H__
#define __ASSISTANT_H__

#include <stdint.h>
#include <stdbool.h>

#define RANGE(a,x,b)        ((a) <= (x) && (x) <= (b))

#define NET_ADDR_STRING_SIZE        (20)

/**
  * @brief Setting network destination address to buffer tail (in AT command format)
  * @param [out] dst - buffer to write net address
  * @param [in] address - pointer to net addtess
  * @return Status code, if set was sucsesseful - return true, other return false;
*/
bool ASSISTANT_sendNetAddress(uint8_t *dst, uint8_t *address);

/**
  * @brief Setting separation string to buffer tail (in AT command format)
  * @param [out] dst - buffer to write separation string
  * @return None
*/
void ASSISTANT_sendSeparator(char *dst);

/**
  * @brief Сhecking for a non-empty address
  * @param [out] addr - buffer to verified address
  * @return verify status. Return true if address is empty 
*/
bool ASSISTANT_isEmptyDestination(uint8_t *addr);

#endif // __ASSISTANT_H__
