#ifndef __PING_H_
#define __PING_H_

#include "SIM800C.h"

void PING_init(void);
void PING_start(void);
void PING_resetTimer(void);

void PING_handle(SIM800_TypeDef* sim800);

#endif // __PING_H_
