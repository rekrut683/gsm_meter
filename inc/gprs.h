#ifndef __GPRS_H__
#define __GPRS_H__

#include "SIM800C.h"

void GPRS_handle(SIM800_TypeDef* sim800);
void GPRS_start(SIM800_TypeDef* sim800);
void GPRS_reset(SIM800_TypeDef* sim800);

#endif // #ifdnef __GPRS_INIT_H__
