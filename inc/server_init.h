#ifndef __SERVER_INIT_H__
#define __SERVER_INIT_H__

#include "SIM800C.h"

void SERVER_INIT_handle(SIM800_TypeDef* sim800);
void SERVER_INIT_start(SIM800_TypeDef* sim800);
void SERVER_INIT_reset(SIM800_TypeDef* sim800);


#endif //__SERVER_INIT_H__
