#ifndef _PUSH_MSG_BUFFER_H
#define _PUSH_MSG_BUFFER_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CONNECTION_STRING_SIZE  (20)
#define PUSH_BUFFER_SIZE        (5)

#pragma pack(push,1)
typedef struct PushMessageStruct
{
    uint8_t	data[64];
    uint8_t size;
} PushMessage;
#pragma pack(pop)

typedef struct
{
    PushMessage msgs[PUSH_BUFFER_SIZE];
    uint8_t size;
    uint16_t idxIn;
    uint16_t idxOut;
} PushMsg_Buffer;

void PUSHBUFFER_init(PushMsg_Buffer* rBuffer);
void pushBuffer_put(PushMsg_Buffer* rBuffer, uint8_t* data, uint8_t count);
PushMessage* pushBuffer_pop(PushMsg_Buffer* rBuffer);
uint8_t pushBuffer_getCount(PushMsg_Buffer* rBuffer);
void PUSHBUFFER_clear(PushMsg_Buffer* rBuffer);

#endif //_PUSH_MSG_BUFFER_H
