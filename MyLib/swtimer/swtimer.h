#ifndef _SWTIMER_H
#define _SWTIMER_H

#include <stdint.h>
#include "../systimer/systimer.h"

#define MAX_SWTIMERS    30

typedef void (*OnSWTimerCallbackType)(void*);

typedef struct SWTimer_struct
{
    void* Context;
    OnSWTimerCallbackType Callback;
    uint32_t InterruptValue;
    volatile uint64_t CurrentValue;
    volatile uint64_t StartValue;
    volatile uint64_t backupValue;
    volatile uint64_t resumeValue;
    volatile uint8_t IsEnabled;
} SWTimer_TypeDef;

typedef struct SWTimerControl_struct
{
	SWTimer_TypeDef* SwTimers[MAX_SWTIMERS];
	uint8_t SwTimersCount;
} SWTimerControl_TypeDef;

void SWTimer_Interrupt();
static void SWTimer_DeleteFromSysTimerList(SWTimer_TypeDef* swtimer);

void SWTimer_Init(SWTimer_TypeDef* swtimer);
void SWTimer_DeInit(SWTimer_TypeDef* swtimer);
void SWTimer_Start(SWTimer_TypeDef* swtimer, uint32_t ticks);
void SWTimer_SubscribeCallback(SWTimer_TypeDef* swtimer, void* context, void (*callback)(void*));
void SWTimer_UnsubscribeCallback(SWTimer_TypeDef* swtimer);
void SWTimer_Stop(SWTimer_TypeDef* swtimer);
void SWTimer_Reset(SWTimer_TypeDef* swtimer);
void SWTimer_Pause(SWTimer_TypeDef* swtimer);
void SWTimer_Resume(SWTimer_TypeDef* swtimer);


#endif