#ifndef _SYSTIMER_H
#define _SYSTIMER_H

#include <stdint.h>
#include "../CMSIS/inc/stm32f0xx.h"
//#include "../swtimer/swtimer.h"

typedef void (*OnSystemTimerTickCallbackType)();

typedef enum SysTimer_enum
{
	SYSTICK_1MS = 0,
	SYSTICK_1US = 1
} SysTimer_Frequency;

typedef struct SysTimer_struct
{
	SysTimer_Frequency				Frequency;
	volatile uint8_t				IsEnabled;
	volatile uint64_t				TickCounter;
	volatile uint64_t				Micros;
	volatile uint64_t				Millis;
	volatile float					Seconds;
	OnSystemTimerTickCallbackType	TickCallback;
} SysTimer_TypeDef;

extern SysTimer_TypeDef SystemTimer;

void SysTimer_Start(SysTimer_Frequency freq);
void SysTimer_Stop();
void SysTimer_SubscribeOnInterrupt(void (*callback)());

uint64_t SysTimer_Millis();
uint64_t SysTimer_Micros();
float SysTimer_Seconds();
uint64_t SysTimer_Ticks();

void SysTimer_DelayMs(uint32_t ms);
void SysTimer_DelayUs(uint32_t us);

#endif