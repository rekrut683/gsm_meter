#include "systimer.h"

extern SysTimer_TypeDef SystemTimer =
{
	.TickCounter	= 0,
	.IsEnabled		= 0,
	.Millis			= 0,
	.Micros			= 0,
	.Seconds		= 0
};

void SysTick_Handler(void)
{
	SystemTimer.TickCounter++;
	if (SystemTimer.Frequency == SYSTICK_1MS)
	{
		SystemTimer.Millis++;
	}
	else if (SystemTimer.Frequency == SYSTICK_1US)
	{
		SystemTimer.Micros++;
	}
	if (SystemTimer.TickCallback)
	{
		SystemTimer.TickCallback();
	}
}

void SysTimer_Start(SysTimer_Frequency freq)
{
	SystemTimer.Frequency = freq;
	SystemTimer.Millis = 0;
	SystemTimer.Micros = 0;
	SystemTimer.Seconds = 0;
	SystemTimer.TickCounter = 0;
	if (freq == SYSTICK_1MS)
	{
		SysTick_Config(SystemCoreClock / 1000);						// 1ms per interrupt
	}
	else if (freq == SYSTICK_1US)
	{
		SysTick_Config(SystemCoreClock / 1000000);					// 1us per interrupt
	}
	else
	{
		return;
	}
	SystemTimer.IsEnabled = 1;
}

void SysTimer_Stop()
{

}

void SysTimer_SubscribeOnInterrupt(void (*callback)())
{
	if (callback)
	{
		SystemTimer.TickCallback = callback;
	}
}

uint64_t SysTimer_Millis()
{
	if (SystemTimer.Frequency == SYSTICK_1MS)
	{
	}
	else if (SystemTimer.Frequency == SYSTICK_1US)
	{
		SystemTimer.Millis = SystemTimer.Micros * 1000;
	}
	return SystemTimer.Millis;
}

uint64_t SysTimer_Micros()
{
	if (SystemTimer.Frequency == SYSTICK_1MS)
	{
		SystemTimer.Micros = SystemTimer.Millis * 1000 + 1000 - SysTick->VAL / 48;
	}
	else if (SystemTimer.Frequency == SYSTICK_1US)
	{
	}
	return SystemTimer.Micros;
}

float SysTimer_Seconds()
{
	if (SystemTimer.Frequency == SYSTICK_1MS)
	{
		SystemTimer.Seconds = (float)SystemTimer.Millis / 1000.0;
	}
	else if (SystemTimer.Frequency == SYSTICK_1US)
	{
		SystemTimer.Seconds = (float)SystemTimer.Micros / 1000000.0;
	}
	return SystemTimer.Seconds;
}

uint64_t SysTimer_Ticks()
{
	return SystemTimer.TickCounter;
}

void SysTimer_DelayMs(uint32_t ms)
{
	uint32_t startMs = SysTimer_Millis();
	while (1)
	{
		if (SysTimer_Millis() - startMs >= startMs + ms)
		{
			break;
		}
	}
}

void SysTimer_DelayUs(uint32_t us)
{
	uint32_t startUs = SysTimer_Micros();
	while (1)
	{
		if (SysTimer_Micros() - startUs >= startUs + us)
		{
			break;
		}
	}
}