#include "watchdog.h"

void Watchdog_Enable(uint8_t prescaler, uint16_t reloadValue)
{
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
	IWDG_SetPrescaler(prescaler);
	IWDG_SetReload(reloadValue);
	IWDG_ReloadCounter();
	IWDG_Enable();
}
void Watchdog_Reset()
{
	IWDG_ReloadCounter();
}