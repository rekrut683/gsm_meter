#include "uart.h"

#define USART_TX_BUFFERSIZE					32

/*
	Get IRQ Type for NVIC
*/
uint8_t GetIRQForUsart(Uart_TypeDef* uart)
{
    if (uart->Usart == USART1)
    {
        return USART1_IRQn;
    }
    else if (uart->Usart == USART2)
    {
        return USART2_IRQn;
    }
    else if (uart->Usart == USART3)
    {
        return USART3_8_IRQn;
    }
    else
    {
        return 0xFF;
    }
}

void USART1_IRQHandler(void)
{
	if (((*USART_1).Usart->ISR & USART_FLAG_RXNE) != RESET)
	{
	  	USART_ClearITPendingBit((*USART_1).Usart, USART_FLAG_RXNE);
		uint8_t rx = USART_ReceiveData((*USART_1).Usart);
		for (int i = 0; i < 5; i++)
		{
			if ((*USART_1).OnReceiveCallbacks[i].callback)
			{
				(*USART_1).OnReceiveCallbacks[i].callback((*USART_1).OnReceiveCallbacks[i].context, rx);
			}
		}
	}
	if (((*USART_1).Usart->ISR & USART_FLAG_ORE) != RESET)
	{
	  	USART_ClearITPendingBit((*USART_1).Usart, USART_FLAG_ORE);
		(*USART_1).Usart->ICR |= USART_ICR_ORECF;
	}
}

void USART2_IRQHandler(void)
{
	if (((*USART_2).Usart->ISR & USART_FLAG_RXNE) != RESET)
	{
	  	USART_ClearITPendingBit((*USART_2).Usart, USART_FLAG_RXNE);
		uint8_t rx = USART_ReceiveData((*USART_2).Usart);
		for (int i = 0; i < 5; i++)
		{
			if ((*USART_2).OnReceiveCallbacks[i].callback)
			{
				(*USART_2).OnReceiveCallbacks[i].callback((*USART_2).OnReceiveCallbacks[i].context, rx);
			}
		}
	}
	if (((*USART_2).Usart->ISR & USART_FLAG_ORE) != RESET)
	{
	  	USART_ClearITPendingBit((*USART_2).Usart, USART_FLAG_ORE);
		(*USART_2).Usart->ICR |= USART_ICR_ORECF;
	}
}


void USART3_6_IRQHandler(void)
{
	if (((*USART_3).Usart->ISR & USART_FLAG_RXNE) != RESET)
	{
	  	USART_ClearITPendingBit((*USART_3).Usart, USART_FLAG_RXNE);
		uint8_t rx = USART_ReceiveData((*USART_3).Usart);
		for (int i = 0; i < 5; i++)
		{
			if ((*USART_3).OnReceiveCallbacks[i].callback)
			{
				(*USART_3).OnReceiveCallbacks[i].callback((*USART_3).OnReceiveCallbacks[i].context, rx);
			}
		}
	}
	if (((*USART_3).Usart->ISR & USART_FLAG_ORE) != RESET)
	{
	  	USART_ClearITPendingBit((*USART_3).Usart, USART_FLAG_ORE);
		(*USART_3).Usart->ICR |= USART_ICR_ORECF;
	}
	if (((*USART_4).Usart->ISR & USART_FLAG_RXNE) != RESET)
	{
	  	USART_ClearITPendingBit((*USART_4).Usart, USART_FLAG_RXNE);
		uint8_t rx = USART_ReceiveData((*USART_4).Usart);
		for (int i = 0; i < 5; i++)
		{
			if ((*USART_4).OnReceiveCallbacks[i].callback)
			{
				(*USART_4).OnReceiveCallbacks[i].callback((*USART_4).OnReceiveCallbacks[i].context, rx);
			}
		}
	}
	if (((*USART_5).Usart->ISR & USART_FLAG_RXNE) != RESET)
	{
	  	USART_ClearITPendingBit((*USART_5).Usart, USART_FLAG_RXNE);
		uint8_t rx = USART_ReceiveData((*USART_5).Usart);
		for (int i = 0; i < 5; i++)
		{
			if ((*USART_5).OnReceiveCallbacks[i].callback)
			{
				(*USART_5).OnReceiveCallbacks[i].callback((*USART_5).OnReceiveCallbacks[i].context, rx);
			}
		}
	}
	if (((*USART_6).Usart->ISR & USART_FLAG_RXNE) != RESET)
	{
	  	USART_ClearITPendingBit((*USART_6).Usart, USART_FLAG_RXNE);
		uint8_t rx = USART_ReceiveData((*USART_6).Usart);
		for (int i = 0; i < 5; i++)
		{
			if ((*USART_6).OnReceiveCallbacks[i].callback)
			{
				(*USART_6).OnReceiveCallbacks[i].callback((*USART_6).OnReceiveCallbacks[i].context, rx);
			}
		}
	}
}

void Usart_Enable(Uart_TypeDef* uart)
{
	*uart->RCC_Register |= uart->RCC_Bus;
}

void Usart_Disable(Uart_TypeDef* uart)
{
	*uart->RCC_Register &= ~uart->RCC_Bus;
}

void Usart_DmaComplete(void* context)
{
	Uart_TypeDef* usart = (Uart_TypeDef*)context;
	usart->TxBusy = 0;
	USART_DMACmd(usart->Usart, USART_DMAReq_Tx, DISABLE);
	DmaChannel_Stop(usart->DmaTxChannel);
	Usart_ClearTxBuffer(usart);
}

void Usart_Init(Uart_TypeDef* usart, uint32_t baudrate)
{
	/* Configure the GPIOs */
  	//Pin_Init(usart->Tx_pin, ALTERNATE, HIGH_SPEED, PUSHPULL, NOPULL);
  	//Pin_Init(usart->Rx_pin, IN, HIGH_SPEED, PUSHPULL, NOPULL);
  
	//Pin_Init(usart->Tx_pin, ALTERNATE_FUNC_PUSH_PULL, HIGH_SPEED);
	//Pin_Init(usart->Rx_pin, THIRD_STATE, HIGH_SPEED);
  
  	Pin_Init_AF(usart->Tx_pin, GPIO_Mode_AF , HIGH_SPEED, PUSHPULL, PUSH_UP, usart->Tx_pin->AF_func);
	
	Pin_Init_AF(usart->Rx_pin, GPIO_Mode_AF, HIGH_SPEED, PUSHPULL, PUSH_UP, usart->Rx_pin->AF_func);

	/* Configure the USART1 */
	USART_InitTypeDef USART_InitStructure;
	
	USART_InitStructure.USART_BaudRate = baudrate;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(usart->Usart, &USART_InitStructure);
	USART_Cmd(usart->Usart, ENABLE);
	
	USART_ITConfig(usart->Usart, USART_IT_RXNE, ENABLE);
	
	usart->Usart->ISR = 0;
	//USART_ITConfig(usart->Usart, USART_IT_TXE, ENABLE);
	//USART_ITConfig(usart->Usart, USART_IT_TC, ENABLE);
	
	//NVIC setup
	uint8_t irq_Ch = GetIRQForUsart(usart);
	if (irq_Ch != 0xFF)
	{
		NVIC_InitTypeDef NVIC_InitStruct;
		NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
		NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
		//NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
		//NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStruct);
	}
	usart->TxBusy = 0;
}

void Usart_ClearTxBuffer(Uart_TypeDef* usart)
{
	for (int i = 0; i < USART_TX_BUFFER_SIZE; i++)
	{
		TxBuffer[i] = '\0';
	}
}

void Usart_SubscribeReceive(Uart_TypeDef* usart, void* context, void(*callback)(void*,uint8_t))
{
	usart->OnReceiveCallbacks->context = context;
	usart->OnReceiveCallbacks->callback = callback;
}

void Usart_Send_Byte(Uart_TypeDef* usart, uint8_t data)
{
  	while(USART_GetFlagStatus(usart->Usart, USART_FLAG_TXE) == RESET);// Wait the set of TXE
  	USART_SendData(usart->Usart, data);  
	while(USART_GetFlagStatus(usart->Usart, USART_FLAG_TC) == RESET);
}

void Usart_Send_String(Uart_TypeDef* usart, uint8_t* str)
{
  	while(*str != 0)
  	{
    		Usart_Send_Byte(usart, *str);
    		str++;
  	}
}

void Usart_Send_Data(Uart_TypeDef* usart, uint8_t* data, uint16_t dataCount)
{
	for (int i = 0; i < dataCount; i++)
	{
		Usart_Send_Byte(usart, data[i]);
	}
}

void UsartTxDma_Init(Uart_TypeDef* usart)
{
	/*DmaChannel_Init(usart->DmaTxChannel,
					USART_TX_BUFFERSIZE,
					(uint32_t)&TxBuffer[0],
					DMA_DIR_TO_PERIPH,
					DMA_MEMORY_SIZE_BYTE,
					DMA_PERIPH_SIZE_BYTE,
					DMA_MODE_NORMAL,
					(uint32_t)usart->UsartTxAddress);*/
	DmaChannel_SubscribeCompleteInterrupt(usart->DmaTxChannel, usart, Usart_DmaComplete);
}

void UsartTxDma_Send(Uart_TypeDef* usart, const uint8_t* data)
{
	while(usart->TxBusy);
	DmaChannel_Init(usart->DmaTxChannel,
					USART_TX_BUFFERSIZE,
					(uint32_t)&TxBuffer[0],
					DMA_DIR_TO_PERIPH,
					DMA_MEMORY_SIZE_BYTE,
					DMA_PERIPH_SIZE_BYTE,
					DMA_MODE_NORMAL,
					(uint32_t)usart->UsartTxAddress);
	DmaChannel_Start(usart->DmaTxChannel);
	uint16_t count = 0;
	while(*(data+count))
	{
		TxBuffer[count] = *(data+count);
		count++;
	}	
	USART_DMACmd(usart->Usart, USART_DMAReq_Tx, ENABLE);
	usart->TxBusy = 1;
}