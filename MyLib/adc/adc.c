#include "adc.h"

/*
	Get IRQ Type for NVIC
*/
uint8_t GetIRQForADC(Adc_TypeDef* adc)
{
    return (adc->Adc == ADC1) ? ADC1_IRQn : 0xFF;
}

void ADC1_IRQHandler()
{
	if (((*ADC_1).Adc->ISR & ADC_FLAG_EOC) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_EOC);
		uint16_t value = ADC_GetConversionValue((*ADC_1).Adc);
		for (int i = 0; i < MAX_ADC_CHANNELS; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				if ((*ADC_1).Channels[i].ConversionCallback[j].callback)
				{
					(*ADC_1).Channels[i].ConversionCallback[j].callback((*ADC_1).Channels[i].ConversionCallback[j].context, value);
				}
			}
		}
		//ADC_StartOfConversion((*ADC_1).Adc);
	}
	if (((*ADC_1).Adc->ISR & ADC_FLAG_ADRDY) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_ADRDY);
	}
	if (((*ADC_1).Adc->ISR & ADC_FLAG_ADCAL) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_ADCAL);
	}
	if (((*ADC_1).Adc->ISR & ADC_FLAG_ADDIS) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_ADDIS);
	}
	if (((*ADC_1).Adc->ISR & ADC_FLAG_ADEN) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_ADEN);
	}
	if (((*ADC_1).Adc->ISR & ADC_FLAG_ADSTART) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_ADSTART);
	}
	if (((*ADC_1).Adc->ISR & ADC_FLAG_ADSTP) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_ADSTP);
	}
	if (((*ADC_1).Adc->ISR & ADC_FLAG_AWD) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_AWD);
	}
	if (((*ADC_1).Adc->ISR & ADC_FLAG_EOSEQ) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_EOSEQ);
	}
	if (((*ADC_1).Adc->ISR & ADC_FLAG_EOSMP) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_EOSMP);
	}
	if (((*ADC_1).Adc->ISR & ADC_FLAG_OVR) != RESET)
	{
		ADC_ClearITPendingBit((*ADC_1).Adc, ADC_FLAG_OVR);
	}
}

uint16_t Adc_GetValue(Adc_TypeDef* adc)
{
	while (ADC_GetFlagStatus(adc->Adc, ADC_FLAG_EOC) == RESET) {}
	return ADC_GetConversionValue(adc->Adc);
}

void Adc_SetChannel(Adc_TypeDef* adc, uint16_t chan, uint8_t samples)
{
	//if (!pin->IsAdcPin && number != 0 && number < MAX_ADC_CHANNELS)
	//{
	//	return;
	//}
	//uint8_t channel = pin->AdcChannel;
	//uint16_t channel = chan;
	//Adc_Channel_TypeDef adcChannel =
	//{
	//	.Number = num,
	//	.Ain = channel
	//};
	//adc->Channels[num - 1] = adcChannel;
	//ADC_RegularChannelConfig(adc->Adc, channel, number, samples);
	ADC_ChannelConfig(adc->Adc, chan, samples);
}

void Adc_Init(Adc_TypeDef* adc, uint32_t divider, uint8_t numberChannels)
{

	ADC_InitTypeDef     ADC_InitStructure;
	*adc->RCC_Register |= adc->RCC_Bus;
	ADC_ClockModeConfig(adc->Adc, divider);
  	ADC_DeInit(adc->Adc);

	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
  	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE; 
  	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
  	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
  	ADC_InitStructure.ADC_ScanDirection = ADC_ScanDirection_Upward;
  	ADC_Init(ADC1, &ADC_InitStructure); 

	//NVIC setup
	uint8_t irq_Ch = GetIRQForADC(adc);
	if (irq_Ch != 0xFF)
	{
		//NVIC_InitTypeDef NVIC_InitStructure;
		//NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
		//NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
		//NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		//NVIC_Init(&NVIC_InitStruct);

		//NVIC_InitStructure.NVIC_IRQChannel = ADC1_IRQn;
  		//NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
  		//NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  		//NVIC_Init(&NVIC_InitStructure);
	}
	//ADC_ITConfig(adc->Adc, ADC_IT_ADRDY, ENABLE);
	//ADC_ITConfig(adc->Adc, ADC_IT_EOC, ENABLE);

}

void Adc_Start(Adc_TypeDef* adc)
{
	ADC_GetCalibrationFactor(adc->Adc);
	ADC_Cmd(adc->Adc, ENABLE);
  	while(!ADC_GetFlagStatus(adc->Adc, ADC_FLAG_ADRDY)); 
  	ADC_StartOfConversion(adc->Adc);
}

void Adc_Stop(Adc_TypeDef* adc)
{
	ADC_StopOfConversion(adc->Adc);
	ADC_Cmd(adc->Adc, DISABLE);
}

void Adc_EnableIRQ(Adc_TypeDef* adc)
{
	ADC_ITConfig(adc->Adc, ADC_IT_EOC, ENABLE);
	//ADC_StartOfConversion(adc->Adc);
}
void Adc_DisableIRQ(Adc_TypeDef* adc)
{
	ADC_ITConfig(adc->Adc, ADC_IT_EOC, DISABLE);
}

void Adc_SubscribeCallback(Adc_TypeDef* adc, uint8_t number, void* context, void(*callback)(void*, uint16_t))
{
	Adc_Channel_TypeDef* channel = NULL;
	for (int i = 0; i < MAX_ADC_CHANNELS; i++)
	{
		if (number == adc->Channels[i].Number)
		{
			channel = &adc->Channels[i];
			break;
		}
	}
	if (channel != NULL)
	{
		channel->ConversionCallback->callback = callback;
		channel->ConversionCallback->context = context;
	}
}

void AdcDma_Init(Adc_TypeDef* adc, uint32_t bufferAdr, uint32_t bufferSize)
{
	DmaChannel_Init(adc->DmaChannel, 
					bufferSize, 
					(uint32_t)bufferAdr, 
					DMA_DIR_FROM_PERIPH, 
					DMA_MEMORY_SIZE_HALFWORD, 
					DMA_PERIPH_SIZE_HALFWORD, 
					DMA_MODE_CIRCULAR, 
					(uint32_t)adc->AdcAddress);
}

void AdcDma_Start(Adc_TypeDef* adc)
{
	ADC_DMACmd(adc->Adc, ENABLE);
}