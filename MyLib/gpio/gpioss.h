#ifndef _GPIOS_H
#define _GPIOS_H

#include "../../StdPeriph/inc/stm32f0xx_gpio.h"

#include <stdint.h>

#define PORTA		&GPORTA
#define PORTB		&GPORTB
#define PORTC		&GPORTC
#define PORTD		&GPORTD
#define PORTF		&GPORTF
#define GPIO_AF		&GGPIO_AF

/*
//	TypeDef Struct of GPIO's
*/
typedef struct GPIOS_Struct
{
	GPIO_TypeDef* 			GPIO;
	uint32_t	 			RCC_Bus;
	volatile uint32_t*		RCC_Register;
	uint8_t					PortSource;
} GPIOS_TypeDef;


/*
//	Public methods for GPIO's
*/
void Port_Enable(GPIOS_TypeDef* gpio);
void Port_Disable(GPIOS_TypeDef* gpio);

/*
// static instances for GPIO's STM32f103
*/
extern GPIOS_TypeDef GPORTA;
extern GPIOS_TypeDef GPORTB;
extern GPIOS_TypeDef GPORTC;
extern GPIOS_TypeDef GPORTD;
extern GPIOS_TypeDef GPORTF;
extern GPIOS_TypeDef GGPIO_AF;

/*
extern GPIOS_TypeDef GPORTA =
{
	.GPIO = GPIOA,
	.RCC_Bus = RCC_APB2Periph_GPIOA,
	.RCC_Register = &RCC->APB2ENR,
	.PortSource = GPIO_PortSourceGPIOA
};

extern GPIOS_TypeDef GPORTB =
{
	.GPIO = GPIOB,
	.RCC_Bus = RCC_APB2Periph_GPIOB,
	.RCC_Register = &RCC->APB2ENR,
	.PortSource = GPIO_PortSourceGPIOB
};

extern GPIOS_TypeDef GPORTC =
{
	.GPIO = GPIOC,
	.RCC_Bus = RCC_APB2Periph_GPIOC,
	.RCC_Register = &RCC->APB2ENR,
	.PortSource = GPIO_PortSourceGPIOC
};

extern GPIOS_TypeDef GGPIO_AF =
{
	.RCC_Bus = RCC_APB2Periph_AFIO,
	.RCC_Register = &RCC->APB2ENR
};*/

#endif