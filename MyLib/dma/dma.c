#include "dma.h"


int GetIRQForDmaChannel(DmaChannel_TypeDef* channel)
{
	if (channel->DmaChannel == DMA1_Channel1)
	{
		return DMA1_Channel1_IRQn;
	}
	else if (channel->DmaChannel == DMA1_Channel2)
	{
		return DMA1_Channel2_3_IRQn;
	}
	else if (channel->DmaChannel == DMA1_Channel3)
	{
		return DMA1_Channel2_3_IRQn;
	}
	else if (channel->DmaChannel == DMA1_Channel4)
	{
		return DMA1_Channel4_5_IRQn;
	}
	else if (channel->DmaChannel == DMA1_Channel5)
	{
		return DMA1_Channel4_5_IRQn;
	}
	else
	{
		return -25;
	}
}

/*
	DMA Interrupts
*/

void DMA1_Channel1_IRQHandler(void)
{
	if (DMA_GetITStatus(DMA1_IT_TC1) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_TC1);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL1).OnDmaComplete[i].callback)
			{
				(*DMA1_CHANNEL1).OnDmaComplete[i].callback((*DMA1_CHANNEL1).OnDmaComplete[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_HT1) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_HT1);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL1).OnHalfTransfer[i].callback)
			{
				((*DMA1_CHANNEL1).OnHalfTransfer[i].callback)((*DMA1_CHANNEL1).OnHalfTransfer[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_TE1) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_TE1);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL1).OnDmaError[i].callback)
			{
				(*DMA1_CHANNEL1).OnDmaError[i].callback((*DMA1_CHANNEL1).OnDmaError[i].context);
			}
		}
	}
}
void DMA1_Channel2_3_IRQHandler(void)
{
	if (DMA_GetITStatus(DMA1_IT_TC2) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_TC2);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL2).OnDmaComplete[i].callback)
			{
				(*DMA1_CHANNEL2).OnDmaComplete[i].callback((*DMA1_CHANNEL2).OnDmaComplete[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_TC2) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_TC2);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL3).OnDmaComplete[i].callback)
			{
				(*DMA1_CHANNEL3).OnDmaComplete[i].callback((*DMA1_CHANNEL3).OnDmaComplete[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_HT2) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_HT2);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL2).OnHalfTransfer[i].callback)
			{
				((*DMA1_CHANNEL2).OnHalfTransfer[i].callback)((*DMA1_CHANNEL2).OnHalfTransfer[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_HT2) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_HT2);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL3).OnHalfTransfer[i].callback)
			{
				((*DMA1_CHANNEL3).OnHalfTransfer[i].callback)((*DMA1_CHANNEL3).OnHalfTransfer[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_TE2) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_TE2);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL2).OnDmaError[i].callback)
			{
				(*DMA1_CHANNEL2).OnDmaError[i].callback((*DMA1_CHANNEL2).OnDmaError[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_TE2) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_TE2);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL3).OnDmaError[i].callback)
			{
				(*DMA1_CHANNEL3).OnDmaError[i].callback((*DMA1_CHANNEL3).OnDmaError[i].context);
			}
		}
	}
}

void DMA1_Channel4_5_IRQHandler(void)
{
	if (DMA_GetITStatus(DMA1_IT_TC4) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_TC4);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL4).OnDmaComplete[i].callback)
			{
				(*DMA1_CHANNEL4).OnDmaComplete[i].callback((*DMA1_CHANNEL4).OnDmaComplete[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_TC4) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_TC4);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL5).OnDmaComplete[i].callback)
			{
				(*DMA1_CHANNEL5).OnDmaComplete[i].callback((*DMA1_CHANNEL5).OnDmaComplete[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_HT4) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_HT4);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL4).OnHalfTransfer[i].callback)
			{
				((*DMA1_CHANNEL4).OnHalfTransfer[i].callback)((*DMA1_CHANNEL4).OnHalfTransfer[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_HT4) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_HT4);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL5).OnHalfTransfer[i].callback)
			{
				((*DMA1_CHANNEL5).OnHalfTransfer[i].callback)((*DMA1_CHANNEL5).OnHalfTransfer[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_TE4) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_TE4);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL4).OnDmaError[i].callback)
			{
				(*DMA1_CHANNEL4).OnDmaError[i].callback((*DMA1_CHANNEL4).OnDmaError[i].context);
			}
		}
	}
	if (DMA_GetITStatus(DMA1_IT_TE4) != RESET)
	{
		DMA_ClearITPendingBit(DMA1_IT_TE4);
		for (int i = 0; i < DMA_CALLBACK_COUNT; i++)
		{
			if ((*DMA1_CHANNEL5).OnDmaError[i].callback)
			{
				(*DMA1_CHANNEL5).OnDmaError[i].callback((*DMA1_CHANNEL5).OnDmaError[i].context);
			}
		}
	}
}


void Dma_Enable(Dma_TypeDef* dma)
{
	*dma->RCC_Register |= dma->RCC_Bus;
}
void Dma_Disable(Dma_TypeDef* dma)
{
	*dma->RCC_Register &= ~dma->RCC_Bus;
}

void DmaChannel_Init(	DmaChannel_TypeDef* channel,
						uint32_t bufSize,
						uint32_t buffer,
						DMA_DIRECTION direction,
						DMA_MEMORY_SIZE	MemorySize,
						DMA_PERIPHERAL_DATASIZE	PeriphDataSize,
						DMA_MODE mode,
						uint32_t periph
					)
{
	DMA_InitTypeDef DMA_InitStructure;

	DMA_InitStructure.DMA_BufferSize = bufSize;
	DMA_InitStructure.DMA_DIR = direction;
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)buffer;
	DMA_InitStructure.DMA_MemoryDataSize = MemorySize;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_Mode = mode;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)periph;
	DMA_InitStructure.DMA_PeripheralDataSize = PeriphDataSize;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_Init(channel->DmaChannel, &DMA_InitStructure);
	//DMA_Cmd(channel->DmaChannel, ENABLE);
	//DMA_ITConfig(channel->DmaChannel, DMA_IT_TC | DMA_IT_HT | DMA_IT_TE, ENABLE);
	//NVIC setup
	int irq_Ch = GetIRQForDmaChannel(channel);
	if (irq_Ch != -25)
	{
		NVIC_InitTypeDef NVIC_InitStruct;
		NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
		//NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
		//NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStruct);
	}
}

void DmaChannel_Start(DmaChannel_TypeDef* channel)
{
	DMA_Cmd(channel->DmaChannel, ENABLE);
	DMA_ITConfig(channel->DmaChannel, DMA_IT_TC | DMA_IT_HT | DMA_IT_TE, ENABLE);
	//NVIC setup
	/*int irq_Ch = GetIRQForDmaChannel(channel);
	if (irq_Ch != -25)
	{
		NVIC_InitTypeDef NVIC_InitStruct;
		NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
		NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
		NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStruct);
	}*/
}

void DmaChannel_Stop(DmaChannel_TypeDef* channel)
{
	//NVIC setup
	int irq_Ch = GetIRQForDmaChannel(channel);
	if (irq_Ch != -25)
	{
		NVIC_InitTypeDef NVIC_InitStruct;
		NVIC_InitStruct.NVIC_IRQChannel = (IRQn_Type)irq_Ch;
		NVIC_InitStruct.NVIC_IRQChannelCmd = DISABLE;
		NVIC_Init(&NVIC_InitStruct);
	}
	DMA_ITConfig(channel->DmaChannel, DMA_IT_TC | DMA_IT_HT | DMA_IT_TE, DISABLE);
	DMA_Cmd(channel->DmaChannel, DISABLE);
}

void DmaChannel_SubscribeCompleteInterrupt(DmaChannel_TypeDef* channel, void* context, void (*callback)(void*))
{
	channel->OnDmaComplete->callback = callback;
	channel->OnDmaComplete->context = context;
}

void DmaChannel_SubscribeErrorInterrupt(DmaChannel_TypeDef* channel, void* context, void (*callback)(void*))
{
	channel->OnDmaError->callback = callback;
	channel->OnDmaError->context = context;
}

void DmaChannel_SubscribeHalfTransferInterrupt(DmaChannel_TypeDef* channel, void* context, void (*callback)(void*))
{
	channel->OnHalfTransfer->callback = callback;
	channel->OnHalfTransfer->context = context;
}