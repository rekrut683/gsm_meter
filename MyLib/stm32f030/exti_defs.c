#include "exti.h"

extern Exti_TypeDef GEXTI_LINE0 =
{
	.ExtiLine			= EXTI_Line0,
	.Callbacks			= {0},
	.Trigger			= EXTI_Trigger_Rising_Falling,
	.ExtiMode			= EXTI_Mode_Interrupt,
	.CallbackCounter	= 0
};

extern Exti_TypeDef GEXTI_LINE1 =
{
	.ExtiLine			= EXTI_Line1,
	.Callbacks			= {0},
	.Trigger			= EXTI_Trigger_Rising_Falling,
	.ExtiMode			= EXTI_Mode_Interrupt,
	.CallbackCounter	= 0
};

extern Exti_TypeDef GEXTI_LINE6 =
{
	.ExtiLine			= EXTI_Line6,
	.Callbacks			= {0},
	.Trigger			= EXTI_Trigger_Rising_Falling,
	.ExtiMode			= EXTI_Mode_Interrupt,
	.CallbackCounter	= 0
};