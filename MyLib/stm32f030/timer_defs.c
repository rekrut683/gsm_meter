#include "timers.h"

extern TIMER_TypeDef GTIMER3 = 
{
	.Timer 				= TIM3,
	.RCC_Bus 			= RCC_APB1Periph_TIM3,
	.RCC_Register			= &RCC->APB1ENR,
	.Prescaler			= 1,
	.Period				= 0,
	.Mode				= COUNTER_UP,
	.Callbacks			= {0},
	.Count_callbacks		= 0,
	.Type 				= GENERAL
};