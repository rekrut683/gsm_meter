#include "assistant.h"

#include <string.h>

const char simCardNotReady[] = {"+CPIN: NOT READY"};

void ASSISTANT_sendSeparator(char *dst)
{
    strcat(dst, "\"");
    strcat(dst, ",");
    strcat(dst, "\"");
}

bool ASSISTANT_sendNetAddress(uint8_t *dst, uint8_t *address)
{
    char *ptr = memchr(address, ':', NET_ADDR_STRING_SIZE);

    if (ptr == NULL)
    {
        return false;
    }

    char* destination = (char*)dst;

    uint8_t addressSize = (uint8_t*)ptr - address;
    strcat(destination, "\"");
    strncat(destination, (const char*)address, addressSize);
    ASSISTANT_sendSeparator(destination);

    strcat(destination, ptr + 1);
    strcat(destination, "\"");
    strcat(destination, "\r\n");

    return true;
}

bool ASSISTANT_isEmptyDestination(uint8_t *addr)
{
    return (addr == NULL || *addr == '*' || *addr == '\0');
}