#include "auto_connect.h"

#include "debug.h"
#include "swtimer.h"

#include "send_data_process.h"
#include "assistant.h"

static SWTimer_TypeDef repeatAutoconnectTimer;
static bool repitionDelayExpiredFlag = false;

#define COMMANDS_AUTO_CONNECT_SIZE (3)

static const CMD_TypeDef commands[COMMANDS_AUTO_CONNECT_SIZE] = 
{
    {"AT+CIPSHUT\r\n",          .resp = {"SHUT OK"},    .badResp = {""},             .error = E_RESET_CON,      .needAddRequest = 0,    .timeoutResponse = 35000,   .needToRetry = 1}, // reset all connections
    {"AT+CIPSTART=\"TCP\",",    .resp = {"CONNECT OK"}, .badResp = {"CONNECT FAIL"}, .error = E_CONTEXT_GPRS,   .needAddRequest = 0,    .timeoutResponse = 15000,   .needToRetry = 0},
    {"AT+CIPSHUT\r\n",          .resp = {"SHUT OK"},    .badResp = {""},             .error = E_RESET_CON,      .needAddRequest = 0,    .timeoutResponse = 35000,   .needToRetry = 1}, // reset all connections
};

static AutoConnectState state = AUTO_CONNECT_STATE_IDLE;
static bool connectionRequireFlag = true;
static uint8_t currentCommand = 0;

static void sendDebugMessage(uint8_t *message)
{
    DEBUG_sendString("\r\n------------------------------\r\n");
    DEBUG_sendString(message);
    DEBUG_sendString("\r\n------------------------------\r\n");
}

static void sendDataCallback(void* context)
{
    if (context)
    {
        SIM800_TypeDef *sim800 = (SIM800_TypeDef*)context;
        sim800->state.autoConnect++;
        SENDDATA_resetCompleteExchangeCallback();
    }
}

static void repeatAutoconnectTimerCallback(void* context)
{
    repitionDelayExpiredFlag = true;
}

static inline bool needConnect(void)
{
    return connectionRequireFlag;
}

static inline void handle(SIM800_TypeDef* sim800)
{
    switch (sim800->state.autoConnect)
    {
        case AUTO_CONNECT_STATE_IDLE:
        case AUTO_CONNECT_STATE_WAIT_INIT_COMPLETE:
        case AUTO_CONNECT_STATE_WAIT_CONNECTION:
        case AUTO_CONNECT_STATE_WAIT_SEND_COMLETE:
        case AUTO_CONNECT_STATE_WAIT_DISCONNECT:
        {
            break;
        }

        case AUTO_CONNECT_STATE_INIT:
        {
            sim800->state.autoConnect++;
            sim800->busyFlag = true;
            sendDebugMessage("* START AUTO CONNECT *");
            sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
            break;
        }
        case AUTO_CONNECT_STATE_CONNECT_TO_DEST:
        {
            sim800->state.autoConnect++;
            uint8_t cmdListen[60] = {0};

            // get base cmd
            strcat(cmdListen, commands[currentCommand].cmd);
            ASSISTANT_sendNetAddress(cmdListen, sim800->autoConnect.destination[2]);
            sendCommand(sim800, &cmdListen[0], commands[currentCommand].timeoutResponse, 0, 0);
            break;
        }
        case AUTO_CONNECT_STATE_SEND_MINE_DESTINATION:
        {
            sim800->state.autoConnect++;
            uint8_t cmdSend[60] = {0};
            sprintf(cmdSend, "<r=0 sn=1234567891234 ip=%s pt=%d>", sim800->ipAddr, LISTEN_PORT);
            rBuffer_putData(sim800->dataTxBuffer, cmdSend, strlen(cmdSend));

            SENDDATA_setCompleteExchangeCallback(sendDataCallback);
            SENDDATA_setExchangeAllowFlag();
            break;
        }
        case AUTO_CONNECT_STATE_DISCONNECT:
        {
            sim800->state.autoConnect++;
            sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
            break;
        }
        case AUTO_CONNECT_STATE_COMPLETED:
        case AUTO_CONNECT_STATE_FAILED:
        {
            if (sim800->state.autoConnect == AUTO_CONNECT_STATE_FAILED)
            {
                sendDebugMessage("* AUTO CONNECT FAILED *");
                SWTimer_Start(&repeatAutoconnectTimer, sim800->autoConnect.repetitionDelay);
                repitionDelayExpiredFlag = false;
            } else
            {
                sendDebugMessage("* AUTO CONNECT COMPLETED *");
                connectionRequireFlag = false;
            }

            sim800->busyFlag = false;
            sim800->state.autoConnect = AUTO_CONNECT_STATE_IDLE;
            SIM800_restoreConnectionState(sim800);
        }
    }
}

void AUTOCONNECT_init(void)
{
    SWTimer_Init(&repeatAutoconnectTimer);
    repitionDelayExpiredFlag = true;
    SWTimer_SubscribeCallback(&repeatAutoconnectTimer, NULL, repeatAutoconnectTimerCallback);
}

void AUTOCONNECT_handle(SIM800_TypeDef* sim800)
{
    if (ASSISTANT_isEmptyDestination(sim800->autoConnect.destination[2]))
    {
        return;
    }

    if (needConnect() && repitionDelayExpiredFlag && sim800->state.autoConnect == AUTO_CONNECT_STATE_IDLE && 
        (sim800->state.global == SIM800_WAITING_MODE || sim800->state.global == SIM800_SERVER_READY ||
        sim800->state.global == SIM800_CLIENT_READY) &&
        rBuffer_getCount(&sim800->rxBuffer) == 0 && 
        (sim800->state.sendData == SIM_SEND_IDLE) &&        // not send data currently
        (sim800->state.receiveData == SIM_RCV_NOT) &&       // not receive data currently
        !sim800->busyFlag)
    {
        SIM800_backupState(sim800);
        SIM800_backupConnectionState(sim800);

        sim800->state.autoConnect = AUTO_CONNECT_STATE_INIT;
        currentCommand = 0;

        sim800->discriptor.cmdContainer = commands;
        sim800->discriptor.cmdPointer   = &currentCommand;
        sim800->discriptor.localState   = &sim800->state.autoConnect;
        sim800->discriptor.failCode     = AUTO_CONNECT_STATE_FAILED;

        sim800->state.global = SIM800_WAITING_MODE;
    }

    handle(sim800);
}

void AUTOCONNECT_enableReconnect(void)
{
    connectionRequireFlag = true;
}