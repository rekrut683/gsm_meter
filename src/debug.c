#include "debug.h"

#include "RingBuffer.h"

RingBuffer_t debugBuffer;

void DEBUG_init(uint16_t size)
{
    rBuffer_init(&debugBuffer, size);
}

void DEBUG_sendString(uint8_t *data)
{
    rBuffer_putString(&debugBuffer, data);
}

void DEBUG_sendData(uint8_t *data, uint16_t size)
{
    rBuffer_putData(&debugBuffer, data, size);
}

uint16_t DEBUG_getDataSize(void)
{
    return rBuffer_getCount(&debugBuffer);
}

uint8_t DEBUG_popByte(void)
{
    return rBuffer_pop(&debugBuffer);
}