#include "ping.h"

#include "swtimer.h"

#include "debug.h"

#define COMMANDS_PING_SIZE      2
#define PING_TIMEOUT            40000

static SWTimer_TypeDef pingTimer;
static bool needPing = false;

static const CMD_TypeDef commands[COMMANDS_PING_SIZE] =
{
    {"AT\r\n",              .resp = {"OK"},         .badResp = {""},    .error = E_CONTEXT_GPRS,    .needAddRequest = 0,    .timeoutResponse = 3000,    .needToRetry = 1},
    {"AT+CIPSTATUS\r\n",    .resp = {"STATE:"},     .badResp = {""},    .error = E_CONTEXT_GPRS,    .needAddRequest = 0,    .timeoutResponse = 3000,    .needToRetry = 1}
};

void pingCallback(void* context)
{
    needPing = true;
    SWTimer_Stop(&pingTimer);
}

void PING_resetTimer(void)
{
    needPing = false;
    SWTimer_Start(&pingTimer, PING_TIMEOUT);
}

static uint8_t currentCommand = 0;

static void sendStateDebug(uint8_t *stateString)
{
    DEBUG_sendString("\r\n------------------------------\r\n");
    DEBUG_sendString(stateString);
    DEBUG_sendString("\r\n------------------------------\r\n"); 
}



void pingProcess(SIM800_TypeDef* sim800)
{
	switch (sim800->state.ping)
	{
	case SIM_PING_OFF:
	case SIM_PING:
	case SIM_PING_IPSTATE_IDLE:
		break;
	case SIM_PING_IDLE:
        sendStateDebug("* PING INIT *");

		if (rBuffer_getCount(&sim800->rxBuffer) == 0)
		{
			sim800->state.ping++;
			sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
		}
		break;
	case SIM_PING_DONE:
		if (sim800->state.global == SIM800_SERVER_READY || sim800->state.global == SIM800_CLIENT_READY)
        {
			sim800->state.ping++;
			sendCommand(sim800, commands[currentCommand].cmd, commands[currentCommand].timeoutResponse, 0, 0);
		} else
		{
			SIM800_restoreState(sim800);
			sim800->state.ping = SIM_PING_OFF;
			currentCommand = 0;
			PING_resetTimer();
		}
		break;
	case SIM_PING_IPSTATE:
        sendStateDebug("* PING COMPLETE *");

		SIM800_restoreState(sim800);

		sim800->state.ping = SIM_PING_OFF;
        currentCommand = 0;
		PING_resetTimer();
		break;
	case SIM_PING_FAILED:
        sendStateDebug("* PING FAILED *");
		SIM800_restoreState(sim800);

		needPing = false;
		sim800->state.ping = SIM_PING_OFF;
		currentCommand = 0;
		break;
	default:
		break;
	}
}

void PING_init(void)
{
    SWTimer_Init(&pingTimer);
    SWTimer_SubscribeCallback(&pingTimer, NULL, pingCallback);
}

void PING_start(void)
{
    SWTimer_Start(&pingTimer, PING_TIMEOUT);
}

void PING_handle(SIM800_TypeDef* sim800)
{
    if (needPing && sim800->state.ping == SIM_PING_OFF &&
        rBuffer_getCount(&sim800->rxBuffer) == 0 && sim800->state.sendData == SIM_SEND_IDLE &&
        sim800->state.receiveData == SIM_RCV_NOT && 
        !sim800->rxWait)
    {
        sim800->state.ping = SIM_PING_IDLE;

        SIM800_backupState(sim800);

        sim800->discriptor.cmdContainer = commands;
        sim800->discriptor.cmdPointer   = &currentCommand;
        sim800->discriptor.localState   = &sim800->state.ping;
        sim800->discriptor.failCode     = SIM_PING_FAILED;

        currentCommand = 0;
    }
    pingProcess(sim800);
}