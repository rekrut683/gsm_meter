#include "SimCmd.h"

const char AT_CMD_END_MARKER        = '\n';
const char INPUT_TEXT_MARKER        = '>';
const char* RECEIVE_DATA_MARKER     = "+IPD";

const char* SIMCARD_NOT_READY   = "+CPIN: NOT READY";
const char* TCP_CON_CLOSED      = "CLOSED";
const char* IP_NOT_READY        = "PDP DEACT";

const CMD_TypeDef SIM800_Sms_Send[COMMANDS_SMS_SIZE] = 
{
    {"AT+CMGF=1\r\n",   .resp = {"OK"},     .badResp = {""},    .error = E_NONE, .needAddRequest = 0,    .timeoutResponse = 3000,    .needToRetry = 1},
    {"AT+CMGS=",        .resp = {">"},      .badResp = {""},    .error = E_NONE, .needAddRequest = 0,    .timeoutResponse = 3000,    .needToRetry = 1},
    {"",                .resp = {0},        .badResp = {""},    .error = E_NONE, .needAddRequest = 0,    .timeoutResponse = 3000,    .needToRetry = 1},
    {"",                .resp = {"OK"},     .badResp = {""},    .error = E_NONE, .needAddRequest = 0,    .timeoutResponse = 3000,    .needToRetry = 1}
};

const CMD_TypeDef SIM800_Client_Cmd[COMMANDS_CLIENT_SIZE] = 
{
    {"AT+CIPSHUT\r\n",          .resp = {"SHUT OK"},    .badResp = {""},                .error = E_RESET_CON,       .needAddRequest = 0,    .timeoutResponse = 35000,   .needToRetry = 1},
    {"AT+CIPSTART=\"TCP\",",    .resp = {"CONNECT OK"}, .badResp = {"CONNECT FAIL"},    .error = E_CONTEXT_GPRS,    .needAddRequest = 0,    .timeoutResponse = 15000,   .needToRetry = 0}
};

const CMD_TypeDef SIM800_Push_Cmd[COMMANDS_PUSH_SIZE] =
{
    {"AT+CIPSHUT\r\n",          .resp = {"SHUT OK"},        .badResp = {""},                .error = E_CONTEXT_GPRS,    .needAddRequest = 0,    .timeoutResponse = 5000,    .needToRetry = 1},
    {"AT+CIPSTART=\"UDP\",",    .resp = {"CONNECT OK"},     .badResp = {"CONNECT FAIL"},    .error = E_CONTEXT_GPRS,    .needAddRequest = 0,    .timeoutResponse = 15000,   .needToRetry = 0},
    {"AT+CIPSHUT\r\n",          .resp = {"SHUT OK"},        .badResp = {""},                .error = E_CONTEXT_GPRS,    .needAddRequest = 0,    .timeoutResponse = 5000,    .needToRetry = 1}
};