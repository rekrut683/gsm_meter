#include "PushMsgBuffer.h"

void PUSHBUFFER_init(PushMsg_Buffer* rBuffer)
{
    rBuffer->size = PUSH_BUFFER_SIZE;
}

void pushBuffer_put(PushMsg_Buffer* rBuffer, uint8_t* data, uint8_t count)
{
    memcpy(rBuffer->msgs[rBuffer->idxIn].data, data, count);
    rBuffer->msgs[rBuffer->idxIn++].size = count;
    if (rBuffer->idxIn >= rBuffer->size)
    {
        rBuffer->idxIn = 0;
    }
}

PushMessage* pushBuffer_pop(PushMsg_Buffer* rBuffer)
{
	PushMessage* retval = &rBuffer->msgs[rBuffer->idxOut++];
	if (rBuffer->idxOut >= rBuffer->size)
	{
		rBuffer->idxOut = 0;
	}
	return retval;
}

uint8_t pushBuffer_getCount(PushMsg_Buffer* rBuffer)
{
	if (rBuffer->idxIn < rBuffer->idxOut)
	{
		return rBuffer->size + rBuffer->idxIn - rBuffer->idxOut;
	}
	else
	{
		return rBuffer->idxIn - rBuffer->idxOut;
	}
}

void PUSHBUFFER_clear(PushMsg_Buffer* rBuffer)
{
    rBuffer->idxIn = 0;
    rBuffer->idxOut = 0;
}
