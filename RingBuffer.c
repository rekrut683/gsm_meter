#include "RingBuffer.h"

#include "stdlib.h"

uint8_t rBuffer_init(RingBuffer_t* rBuffer, uint16_t size)
{
    rBuffer->buffer = (uint8_t*)malloc(size * sizeof(uint8_t));
    rBuffer->size = size;

    return rBuffer->buffer != NULL;
}

void rBuffer_put(RingBuffer_t* rBuffer, uint8_t data)
{
	rBuffer->buffer[rBuffer->idxIn++] = data;
	if (rBuffer->idxIn >= rBuffer->size) 
	{
		rBuffer->idxIn = 0;
	}
}

uint8_t rBuffer_pop(RingBuffer_t* rBuffer)
{
	uint8_t byte = rBuffer->buffer[rBuffer->idxOut++];
	if (rBuffer->idxOut >= rBuffer->size)
	{
	  	rBuffer->idxOut = 0;	
	}
	return byte;
}

uint16_t rBuffer_getCount(RingBuffer_t* rBuffer)
{
	uint16_t retval = 0;
	if (rBuffer->idxIn < rBuffer->idxOut)
	{
	  	retval = rBuffer->size + rBuffer->idxIn - rBuffer->idxOut;
	}
	else
	{
	  	retval = rBuffer->idxIn - rBuffer->idxOut;
	}
	return retval;
}

uint8_t rBuffer_showByte(RingBuffer_t* rBuffer, uint16_t position)
{
	uint16_t pointer = rBuffer->idxOut + position;
	uint8_t byte = 0xFF;
	if (position < rBuffer_getCount(rBuffer))
	{
	  	if (pointer > rBuffer->size)
		{
		  	pointer -= rBuffer->size;
		}
		byte = rBuffer->buffer[pointer];
	}
	return byte;
}

void rBuffer_clear(RingBuffer_t* rBuffer)
{
    rBuffer->idxIn = 0;
    rBuffer->idxOut = 0;
}

void rBuffer_putString(RingBuffer_t* rBuffer, uint8_t* string)
{
    while (*string != '\0')
    {
        rBuffer_put(rBuffer, *string++);
    }
}

void rBuffer_putData(RingBuffer_t* rBuffer, uint8_t* data, uint16_t dataCount)
{
    for (uint16_t i = 0; i < dataCount; i++)
    {
        rBuffer_put(rBuffer, data[i]);
    }
}