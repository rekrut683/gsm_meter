#ifndef _PACKET_BUFFER_H
#define _PACKET_BUFFER_H

#include <stdint.h>

#define MAX_PACKET_SIZE				128
#define	MAX_PACKETS_BUFFER_SIZE		10

typedef struct PACKET_BUFFER_STRUCT {
	uint8_t packet[MAX_PACKET_SIZE];
	uint8_t pointer;
	uint8_t countBytes;
} PacketInstance_t;


typedef struct
{
	PacketInstance_t packet[MAX_PACKETS_BUFFER_SIZE];
	uint16_t idxIn;
	uint16_t idxOut;
	uint16_t size;
} Packet_Buffer;

void packetBuffer_init(Packet_Buffer* rBuffer);

void packetBuffer_put(Packet_Buffer* rBuffer, uint8_t* data, uint8_t count);
PacketInstance_t* packetBuffer_pop(Packet_Buffer* rBuffer);
uint16_t packetBuffer_getCount(Packet_Buffer* rBuffer);
uint8_t packetBuffer_getPosition(Packet_Buffer* rBuffer);
uint8_t packetBuffer_putByte(Packet_Buffer* rBuffer, uint8_t data, uint8_t position);
void packetBuffer_setPacket(Packet_Buffer* rBuffer, uint8_t dataCount, uint8_t position);

#endif