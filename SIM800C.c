#include "SIM800C.h"

#include "assistant.h"

#include "init_sim800.h"
#include "gprs.h"
#include "server_init.h"
#include "ping.h"
#include "auto_connect.h"
#include "send_data_process.h"
#include "debug.h"

void SIM800_Init(SIM800_TypeDef* sim800, RingBuffer_t* rxExternBuffer, RingBuffer_t* txExternBuffer)
{
	// enable gsm power & reset pins
	Pin_Init(sim800->powerPin, GPIO_Mode_OUT, HIGH_SPEED, PUSHPULL, NOPULL);
	Pin_Clear(sim800->powerPin);
	Pin_Init(sim800->resetPin, GPIO_Mode_OUT, HIGH_SPEED, PUSHPULL, NOPULL);
	Pin_Clear(sim800->resetPin);
	// ring buffer init
	rBuffer_init(&sim800->rxBuffer, BUFFER_SIZE);
	rBuffer_init(&sim800->txBuffer, BUFFER_SIZE);
	rBuffer_init(&sim800->dataBytesBuffer, 10);
	initSMSBuffer(&sim800->smsBuffer, 10);
	PUSHBUFFER_init(&sim800->pushBuffer);
	packetBuffer_init(&sim800->packetBuffer);
	// enable uart gsm module
	Usart_Enable(sim800->serial);
	Usart_Init(sim800->serial, BAUDRATE);
	Usart_SubscribeReceive(sim800->serial, sim800, OnSerialRead);
	// debuf buffer
	// extern device buffers
	sim800->dataRxBuffer = rxExternBuffer;
	sim800->dataTxBuffer = txExternBuffer;
	// timers init
	SWTimer_Init(&sim800->powerTimer);
	SWTimer_Init(&sim800->commandTimer);
	SWTimer_Init(&sim800->smsTimer);
	SWTimer_SubscribeCallback(&sim800->powerTimer, sim800, powerUpTimer);
	SWTimer_SubscribeCallback(&sim800->commandTimer, sim800, programInitTimer);
	SWTimer_SubscribeCallback(&sim800->smsTimer, sim800, smsInitTimer);

 	PING_init();

	sim800->isRun = 0;
	sim800->isOnOff = 0;
}

void clearTempBuffer(SIM800_TypeDef* sim800);

void SIM800_Start(SIM800_TypeDef* sim800)
{
    Pin_Set(sim800->powerPin);

    sim800->state.powerUp = SIM800_POWER_OFF;
    sim800->state.programInit = SIM800_PROGRAM_OFF;
    sim800->state.gprsInit = SIM_GPRS_OFF;
    sim800->state.serverInit = SIM_SERVER_OFF;

    INIT_SIM800_reset(sim800);

    PING_start();
    sim800->isRun = 1;
}

void SIM800_reboot(SIM800_TypeDef* sim800)
{
    sim800->state.powerUp = SIM800_POWER_OFF;
    sim800->state.programInit = SIM800_PROGRAM_OFF;
    sim800->state.gprsInit = SIM_GPRS_OFF;
    sim800->state.serverInit = SIM_SERVER_OFF;

    INIT_SIM800_reset(sim800);

    PING_resetTimer();
    sim800->isRun = 1;
}

void SIM800_Stop(SIM800_TypeDef* sim800)
{
	sim800_off(sim800);
	sim800->state.global = SIM800_OFF;
	Pin_Clear(sim800->powerPin);
}

SIM800_GLOBAL_STATE SIM800_GetState(SIM800_TypeDef* sim800)
{
	return sim800->state.global;
}

void checkTxBuffer(SIM800_TypeDef* sim800);

void SIM800_Routine(SIM800_TypeDef* sim800)
{
    if (sim800->isRun) 	// check isRun flag
    {
        checkRxBuffer(sim800);
        checkTxBuffer(sim800);

        SENDDATA_handle(sim800);

        checkSms(sim800);
        checkPush(sim800);
        PING_handle(sim800);
        checkReceivePackets(sim800);

        AUTOCONNECT_handle(sim800);

		switch (sim800->state.global)
		{
			case SIM800_INIT_PROCESS:   //sim800 is off
				powerUpprocess(sim800);
				break;
			case SIM800_INIT:           //sim800 power up, but not porgram init
			{
				INIT_SIM800_handle(sim800);
				break;
			}
			case SIM800_READY:          //sim800 power up & program ready
			{
				GPRS_start(sim800);
				break;
			}
			case SIM800_GPRS_PROCESS:
			{
				GPRS_handle(sim800);
				break;
			}
			case SIM800_GPRS_READY:
			{
				SERVER_INIT_start(sim800);
				break;
			}
			case SIM800_SERVER_INIT_PROCESS:
			{
				SERVER_INIT_handle(sim800);
				break;
			}
		case SIM800_CLIENT_INIT_PROCESS:
			clientInitProcess(sim800);
			break;
		case SIM800_WAITING_MODE:
			break;
		}
	}
}

/**********************CALLBACKS*******************************/
void SIM800_SetOnClientInitCallback(SIM800_TypeDef* sim800, void (*callback)(void*, SIM800_ERROR))
{
	sim800->callbacks.onSim800ClientInit = callback;
}
void SIM800_SetOnDataReceiveCallback(SIM800_TypeDef* sim800, void (*callback)(void*, uint16_t))
{
	sim800->callbacks.onSim800DataReceive = callback;
}
void SIM800_SetOnNotResponseCallback(SIM800_TypeDef* sim800, void(*callback)(void*))
{
	sim800->callbacks.onSim800NotResponse = callback;
}
void SIM800_SetOnGaspFinished(SIM800_TypeDef* sim800, void(*callback)(void*, SIM800_ERROR))
{
	sim800->callbacks.onSim800GaspFinished = callback;
}

/****************************????????*****************************/
void sim800_off(SIM800_TypeDef* sim800)
{
	SIM800_ProgramReset(sim800);
	sim800->isRun = 0;
}

void killConnections(SIM800_TypeDef* sim800) {
	SIM800_ClientReset(sim800);
	SIM800_ServerReset(sim800);
	sim800->state.global = SIM800_WAITING_MODE;
}

void SIM800_ProgramReset(SIM800_TypeDef* sim800)
{
    SIM800_GprsReset(sim800);
    INIT_SIM800_reset(sim800);
    SIM800_resetFlags(sim800);
}

void SIM800_GprsReset(SIM800_TypeDef* sim800)
{
    SIM800_ServerReset(sim800);         // reset server, if its
    SIM800_ClientReset(sim800);         // reset client, if its
    GPRS_reset(sim800);
    SIM800_resetFlags(sim800);          // reset all flags Sim800
}

/************************Client functions*********************************/
void SIM800_Client_Start(SIM800_TypeDef* sim800)
{
    sim800->state.global = SIM800_CLIENT_INIT_PROCESS;

    sim800->discriptor.failCode     = SIM_CLIENT_FAILED;
    sim800->discriptor.cmdContainer = &SIM800_Client_Cmd[0];
    sim800->discriptor.cmdPointer   = &sim800->clientCmdPointer;
    sim800->discriptor.localState   = &sim800->state.clientInit;
}

void clientInitProcess(SIM800_TypeDef* sim800)
{
	switch (sim800->state.clientInit)
	{
	case SIM_CLIENT_OFF:
		if (rBuffer_getCount(&sim800->rxBuffer) == 0)
		{
			sim800->state.clientInit++;
			sendCommand(sim800, &SIM800_Client_Cmd[sim800->clientCmdPointer].cmd[0], SIM800_Client_Cmd[sim800->clientCmdPointer].timeoutResponse, 0, 0);
		}
		break;
	case SIM_CLIENT_RST_CON_IDLE:
		break;
	case SIM_CLIENT_RST_CON:
		sim800->state.clientInit++;

		uint8_t cmdListen[60] = { 0 };

		// get port
		uint8_t stringNumPort[16];
		sprintf(stringNumPort, "%d", SERVER_PORT);
		// get base cmd
		strcat(cmdListen, &SIM800_Client_Cmd[sim800->clientCmdPointer].cmd[0]);

		strcat(cmdListen, "\"");
		strcat(cmdListen, SERVER_ADDR);
		strcat(cmdListen, "\"");
		strcat(cmdListen, ",");
		strcat(cmdListen, "\"");

		strcat(cmdListen, stringNumPort);
		strcat(cmdListen, "\"");
		strcat(cmdListen, "\r\n");

		sendCommand(sim800, &cmdListen[0], SIM800_Client_Cmd[sim800->clientCmdPointer].timeoutResponse, 0, 0);
		break;
	case SIM_CLIENT_START_IDLE:
		break;
	case SIM_CLIENT_START:
		sim800->state.global = SIM800_CLIENT_READY;
		sim800->state.clientInit++;
		if (sim800->callbacks.onSim800ClientInit)
		{
			sim800->callbacks.onSim800ClientInit(0, E_NONE);
		}
		break;
	case SIM_CLIENT_FAILED:
		SIM800_ClientReset(sim800);
		if (sim800->callbacks.onSim800ClientInit)
		{
			sim800->callbacks.onSim800ClientInit(0, SIM800_Client_Cmd[sim800->clientCmdPointer].error);
		}
		break;
	}
}

static StateDescriptor backupState;

void SIM800_backupState(SIM800_TypeDef* sim800)
{

    backupState.localState      = sim800->discriptor.localState;
    backupState.cmdContainer    = sim800->discriptor.cmdContainer;
    backupState.cmdPointer      = sim800->discriptor.cmdPointer;
}

void SIM800_restoreState(SIM800_TypeDef* sim800)
{
    sim800->discriptor.localState   = backupState.localState;
    sim800->discriptor.cmdPointer   = backupState.cmdPointer;
    sim800->discriptor.cmdContainer = backupState.cmdContainer;
}

void SIM800_ClientReset(SIM800_TypeDef* sim800)
{
	sim800->clientCmdPointer = 0;
	sim800->state.clientInit = SIM_CLIENT_OFF;
	sim800->state.global = SIM800_CLIENT_INIT_PROCESS;
	SIM800_resetFlags(sim800);
}

void SIM800_ServerReset(SIM800_TypeDef* sim800)
{
    SERVER_INIT_reset(sim800);
    SIM800_resetFlags(sim800);
}

/*****************************Ping Sim800*************************************/

/******************************PowerUp Sim800********************************/
void powerUpprocess(SIM800_TypeDef* sim800)
{
    switch (sim800->state.powerUp)
    {
        case SIM800_POWER_OFF:			// delay before sim800 on
            powerUpStep(sim800, DELAY_POWER_UP);
            break;
        case SIM800_POWER_ON_IDLE:		// waiting for delay before sim800 on
        case SIM800_POWER_RESET_IDLE:	// waiting for sim800 is reboot
        case SIM800_POWER_READY_IDLE:	// waiting for delay before Ready sim800
            break;
        case SIM800_POWER_ON:			// powerkey pin to "LOW"
            Pin_Set(sim800->resetPin);
            powerUpStep(sim800, DELAY_RESET_UP);
            break;
        case SIM800_POWER_RESET:		// powekey pin to "HIGH" reboot is over
            Pin_Clear(sim800->resetPin);
            powerUpStep(sim800, DELAY_WAITING_INIT);
            break;
        case SIM800_POWER_READY:		// sim800 power up ready
            if (sim800->isOnOff)
            {
                INIT_SIM800_reset(sim800);
                sim800->isOnOff = 0;
            } else
            {
                sim800->isOnOff = 1;
                sim800->state.global = SIM800_INIT;
            }
            break;
        default:
            break;
	}
}

void powerUpTimer(void* context)
{
	SIM800_TypeDef* sender = (SIM800_TypeDef*)context;
	if (sender)
	{
		SWTimer_Stop(&sender->powerTimer);
		sender->state.powerUp++;
	}
}

void powerUpStep(SIM800_TypeDef* sim800, uint32_t delay)
{
	sim800->state.powerUp++;
	SWTimer_Start(&sim800->powerTimer, delay);
}

/************GASP sim800******************/
void SIM800_StartGasp(SIM800_TypeDef* sim800)
{
	if (sim800->callbacks.onSim800GaspFinished)
	{
		sim800->callbacks.onSim800GaspFinished(0, 0);
	}
}

void gaspProcess(SIM800_TypeDef* sim800)
{

}

/************PUSH MSGS sim800***********************************************/
void checkPush(SIM800_TypeDef* sim800)
{
    if (ASSISTANT_isEmptyDestination(sim800->pushSetupDest.address))
    {
        return;
    }

    if (pushBuffer_getCount(&sim800->pushBuffer) != 0 &&    // push buffer isnt empty
        sim800->state.push == SIM_PUSH_OFF &&               // current push state is off
        (sim800->state.global == SIM800_SERVER_READY || sim800->state.global == SIM800_CLIENT_READY ||
         sim800->state.global == SIM800_WAITING_MODE) &&    // server or client state active

        rBuffer_getCount(&sim800->rxBuffer) == 0 &&         // rx buffer is empty
        (sim800->state.sendData == SIM_SEND_IDLE) &&         // not send data currently
        (sim800->state.receiveData == SIM_RCV_NOT) &&       // not receive data currently
        packetBuffer_getCount(&sim800->packetBuffer) == 0 &&
        !sim800->busyFlag)
    {
        sim800->state.push = SIM_PUSH_BACKUP_PREV_STATE_IDLE;

        SIM800_backupState(sim800);
        SIM800_backupConnectionState(sim800);

        sim800->discriptor.localState   = &sim800->state.push;
        sim800->discriptor.failCode     = SIM_PUSH_FAILED;
        sim800->discriptor.cmdContainer = SIM800_Push_Cmd;
        sim800->discriptor.cmdPointer   = &sim800->pushPointer;

        sim800->pushPointer = 0;
        sim800->state.global = SIM800_WAITING_MODE;
    }

    pushMsgProcess(sim800);
}

void pushMsgProcess(SIM800_TypeDef* sim800)
{
	switch (sim800->state.push)
	{
	case SIM_PUSH_OFF:
		break;
	case SIM_PUSH_BACKUP_PREV_STATE_IDLE:
		if (rBuffer_getCount(&sim800->rxBuffer) == 0)
		{
			sim800->state.push++;
		}
		sim800->busyFlag = true;
		break;
	case SIM_PUSH_BACKUP_PREV_STATE:
		DEBUG_sendString("\r\n------------------------------\r\n");
		DEBUG_sendString("* START PUSH *");
		DEBUG_sendString("\r\n------------------------------\r\n");
		sim800->state.push++;
		sendCommand(sim800, &SIM800_Push_Cmd[sim800->pushPointer].cmd[0], SIM800_Push_Cmd[sim800->pushPointer].timeoutResponse, 0, 0);
		break;
	case SIM_PUSH_SHUTDOWN_CONN_IDLE:
		break;
	case SIM_PUSH_SHUTDOWN_CONN:
		sim800->state.push++;
		killConnections(sim800);
		break;
	case SIM_PUSH_RESET_STATES_IDLE:
		sim800->state.push++;
		break;
	case SIM_PUSH_RESET_STATES:
		sim800->state.push++;
		// get current push message
		PushMessage *currentMessage = pushBuffer_pop(&sim800->pushBuffer);

		memset(&sim800->currentPushMessage, 0, sizeof(sim800->currentPushMessage));

		memcpy(sim800->currentPushMessage.data, currentMessage->data, currentMessage->size);
		sim800->currentPushMessage.size = currentMessage->size;

		if (!sim800->notifyServerConnect)
		{
			uint8_t cmdListen[50] = {0};

			uint8_t *ptr = memchr(sim800->pushSetupDest.address, ':', NET_ADDR_STRING_SIZE);

			if (ptr != NULL)
			{
				strcat(cmdListen, &SIM800_Push_Cmd[sim800->pushPointer].cmd[0]);
				strcat(cmdListen, "\"");

				uint8_t destSize = ptr - sim800->pushSetupDest.address;
				strncat(cmdListen, sim800->pushSetupDest.address, destSize);
				ASSISTANT_sendSeparator(cmdListen);
				strcat(cmdListen, ptr + 1);
				strcat(cmdListen, "\"");
				strcat(cmdListen, "\r\n");

				sendCommand(sim800, &cmdListen[0], SIM800_Push_Cmd[sim800->pushPointer].timeoutResponse, 0, 0);
			}
		}
		else
		{
			sim800->state.push++;
			sim800->pushPointer++;
		}
		break;
	case SIM_PUSH_CONNECT_IDLE:
		break;
	case SIM_PUSH_CONNECT:
		sim800->state.push++;
		sim800->notifyServerConnect = 1;
		for (uint8_t i = 0; i < sim800->currentPushMessage.size; i++)
		{
			rBuffer_put(sim800->dataTxBuffer, sim800->currentPushMessage.data[i]);
		}
		
		SENDDATA_setCompleteExchangeCallback(pushSendDataCallback);
		SENDDATA_setExchangeAllowFlag();
		break;
	case SIM_PUSH_SEND_IDLE:
		break;
	case SIM_PUSH_SEND:
		if (pushBuffer_getCount(&sim800->pushBuffer) > 0) {
			sim800->state.push = SIM_PUSH_RESET_STATES;
			sim800->pushPointer--;
		} else
		{
			sim800->state.push++;
			sendCommand(sim800, &SIM800_Push_Cmd[sim800->pushPointer].cmd[0], SIM800_Push_Cmd[sim800->pushPointer].timeoutResponse, 0, 0);
		}
		break;
	case SIM_PUSH_DISCONNECT_IDLE:
		break;
	case SIM_PUSH_DISCONNECT:
		DEBUG_sendString("\r\n------------------------------\r\n");
		DEBUG_sendString("* PUSH COMPLETED *");
		DEBUG_sendString("\r\n------------------------------\r\n");

		sim800->busyFlag = false;

		sim800->state.push = SIM_PUSH_OFF;
		sim800->notifyServerConnect = 0;
		SIM800_restoreConnectionState(sim800);
		break;
	case SIM_PUSH_FAILED:
		sim800->pushPointer++;
		DEBUG_sendString("\r\n------------------------------\r\n");
		DEBUG_sendString("* PUSH FAILED *");
		DEBUG_sendString("\r\n------------------------------\r\n");

		sim800->busyFlag = false;

		sim800->state.push = SIM_PUSH_SEND;
		PUSHBUFFER_clear(&sim800->pushBuffer);
		break;
	default:
		break;
	}
}

static uint8_t connectionStateBackup = 0;

void SIM800_backupConnectionState(SIM800_TypeDef* sim800)
{
    connectionStateBackup = sim800->state.global;
}

void SIM800_restoreConnectionState(SIM800_TypeDef* sim800)
{
    switch (connectionStateBackup)
    {
    case SIM800_SERVER_READY:
        SERVER_INIT_start(sim800);
        break;
    case SIM800_CLIENT_READY:
        sim800->discriptor.localState   = &sim800->state.clientInit;
        sim800->discriptor.cmdContainer = SIM800_Client_Cmd;
        sim800->discriptor.cmdPointer   = &sim800->clientCmdPointer;

        sim800->state.global = SIM800_CLIENT_INIT_PROCESS;
        break;
    default:
        break;
    }

    connectionStateBackup = 0;
}

void pushSendDataCallback(void* context)
{
    SIM800_TypeDef* sim800 = (SIM800_TypeDef*)context;
    if (sim800)
    {
        sim800->state.push++;
		SENDDATA_resetCompleteExchangeCallback();
    }
}

/**************************SMS Send process*********************************/
void SIM800_SendSms(SIM800_TypeDef* sim800, uint8_t* tel, uint8_t* text)
{
    SMS_Instance sms;
    sms.message = text;
    sms.number = tel;
    smsBuffer_put(&sim800->smsBuffer, sms);
}

void checkSms(SIM800_TypeDef* sim800)
{
    if (smsBuffer_getCount(&sim800->smsBuffer) != 0 && sim800->state.smsInit == SIM_SMS_OFF)
    {
        sim800->state.smsInit = SIM_SMS_TEXT_MODE_PROCESS;
        sim800->currentSms = smsBuffer_pop(&sim800->smsBuffer);

        SIM800_backupState(sim800);

        sim800->discriptor.localState   = &sim800->state.smsInit;
        sim800->discriptor.cmdContainer = SIM800_Sms_Send;
        sim800->discriptor.cmdPointer   = &sim800->smsCmdPointer;
    }
    smsSendProcess(sim800);
}

void smsSendProcess(SIM800_TypeDef* sim800)
{
	switch (sim800->state.smsInit)
	{
	case SIM_SMS_TEXT_MODE_PROCESS:
		if (rBuffer_getCount(&sim800->rxBuffer) == 0)
		{
			sim800->state.smsInit++;
			sendCommand(sim800, &SIM800_Sms_Send[sim800->smsCmdPointer].cmd[0], SIM800_Sms_Send[sim800->smsCmdPointer].timeoutResponse, 0, 0);
		}
		break;
	case SIM_SMS_TEXT_MODE:
		break;
	case SIM_SMS_TEL_PROCESS:
		sim800->state.smsInit++;
		uint8_t cmd[60] = { 0 };
		strcat(cmd, &SIM800_Sms_Send[sim800->smsCmdPointer].cmd[0]);
		strcat(cmd, "\"");
		strcat(cmd, sim800->currentSms.number);
		strcat(cmd, "\"");
		strcat(cmd, "\r\n");
		sendCommand(sim800, &cmd[0], SIM800_Sms_Send[sim800->smsCmdPointer].timeoutResponse, 0, 0);
		break;
	case SIM_SMS_TEL:
		break;
	case SIM_SMS_MESSAGE_PROCESS:
		sim800->state.smsInit++;
		uint8_t cmd2[256] = { 0 };
		strcat(cmd2, sim800->currentSms.message);
		strcat(cmd2, "\032");
		sim800->smsCmdPointer++;
		sendCommand(sim800, &cmd2[0], TIMEOUT_RESPONSE, 0, 0);
		break;
	case SIM_SMS_MESSAGE:
		break;
	case SIM_SMS_END_PROCESS:
		sim800->state.smsInit++;
		break;
	case SIM_SMS_END:
		sim800->state.smsInit++;
		break;
	case SIM_SMS_FINISH:
		sim800->state.smsInit = SIM_SMS_OFF;
		SIM800_restoreState(sim800);
		break;
	}
}

void smsInitTimer(void* context)
{
	SIM800_TypeDef* sim800 = (SIM800_TypeDef*)context;
	if (sim800)
	{
		SWTimer_Stop(&sim800->commandTimer);
		if (sim800->state.smsInit != SIM_SMS_OFF && rBuffer_getCount(&sim800->rxBuffer) == 0 &&
			!sim800->statusResponse &&
			sim800->rxWait == 1)
		{
			if (sim800->countAttempt < COUNT_OF_ATTEMPT - 1)
			{
				SWTimer_Start(&sim800->commandTimer, TIMEOUT_RESPONSE);
				const uint8_t* cmd;
				cmd = SIM800_Sms_Send[sim800->smsCmdPointer].cmd;
				rBuffer_putString(&sim800->txBuffer, (uint8_t*)cmd);
				sim800->countAttempt++;
			}
		}
	}
}

/****************************Commands Sim800**********************************/
void programInitTimer(void* context)
{
	SIM800_TypeDef* sim800 = (SIM800_TypeDef*)context;
	if (sim800)
	{
		resendCmd(sim800);
	}
}

void sendCommand(SIM800_TypeDef* sim800, const uint8_t* cmd, uint32_t timeout, uint8_t needAnswer, int countData)
{
    SWTimer_Start(&sim800->commandTimer, timeout);
    sim800->statusResponse = 0;
    sim800->rxWait = 1;
    if(countData == 0)
    {
        rBuffer_putString(&sim800->txBuffer, (uint8_t*)cmd);
    }
    else
    {
        rBuffer_putData(&sim800->txBuffer, (uint8_t*)cmd, countData);
    }
    
    strcpy(sim800->lastCmd, cmd);
    if (needAnswer)
    {
        sim800->isNeedResponseValue = 1;
    }
}

void resendCmd(SIM800_TypeDef* sim800)
{
    DEBUG_sendString("resend cmd\r\n");
    SWTimer_Stop(&sim800->commandTimer);

    if (!sim800->discriptor.cmdContainer[*sim800->discriptor.cmdPointer].needToRetry)
    {
        *sim800->discriptor.localState = sim800->discriptor.failCode;
        return;
    }

    if (rBuffer_getCount(&sim800->rxBuffer) != 0 ||
        sim800->state.sendData != SIM_SEND_IDLE && sim800->state.receiveData != SIM_RCV_NOT)
    {
        DEBUG_sendString("restart timer");
        SWTimer_Start(&sim800->commandTimer, TIMEOUT_RESPONSE);
        return;
    }

    if (RANGE(SIM800_INIT, sim800->state.global, SIM800_CLIENT_READY) && !sim800->statusResponse && sim800->rxWait == 1)
    {
        uint8_t message[30] = {0};
        sprintf(message, "Repit command attempt: %u\r\n", sim800->countAttempt + 1);
        DEBUG_sendString(message);

        if (sim800->countAttempt < COUNT_OF_ATTEMPT - 1)
        {
            SWTimer_Start(&sim800->commandTimer, TIMEOUT_RESPONSE);
            uint8_t* cmd = sim800->lastCmd;
            rBuffer_putString(&sim800->txBuffer, cmd);
            sim800->countAttempt++;
        } else
        {
              *sim800->discriptor.localState = sim800->discriptor.failCode;
        }
    }
}

/**************************Uart Sim800 Buffers********************************/
void checkTxBuffer(SIM800_TypeDef* sim800)
{
    uint16_t dataCount = rBuffer_getCount(&sim800->txBuffer);
    if (dataCount > 0)
    {
        uint8_t d = rBuffer_pop(&sim800->txBuffer);
        Usart_Send_Byte(sim800->serial, d);
    }
}

void checkRxBuffer(SIM800_TypeDef* sim800)
{
    if (rBuffer_getCount(&sim800->rxBuffer) > 0)
    {
        PING_resetTimer();

        uint8_t d = rBuffer_pop(&sim800->rxBuffer);
#ifdef FULL_LOG_SIM800
        DEBUG_sendData(&d, sizeof(d));
#endif
        sim800->tempBuffer[sim800->tempBufferPointer++] = d;

        if (sim800->state.global == SIM800_SERVER_READY || sim800->state.global == SIM800_CLIENT_READY)
        {
            parseGprsRcvData(sim800, d);
        }

        if (d == AT_CMD_END_MARKER || d == INPUT_TEXT_MARKER)
        {
            if (parseErrorReceive(sim800))
            {
				clearTempBuffer(sim800);
                return;
            }
#ifdef DOT_LOG_SIM800
        	DEBUG_sendString(".");
#endif
            sim800->tempBuffer[sim800->tempBufferPointer] = '\0';
            if (sim800->discriptor.cmdContainer != 0)
            {
                const CMD_TypeDef* cmdTarget = sim800->discriptor.cmdContainer;
                uint8_t pointer = *sim800->discriptor.cmdPointer;
                uint8_t result = SIM800_RESPONSE_NOT;
                for (uint8_t i = 0; i < RESPONSE_ARRAY_SIZE; i++)
                {
                    if (cmdTarget[pointer].resp[i][0] != '\0')
                    {
                        if (strstr(sim800->tempBuffer, &cmdTarget[pointer].resp[i][0]) != NULL)
                        {
                            result = SIM800_RESPONSE_GOOD;
                            break;
                        }
                    }
                    if (result != SIM800_RESPONSE_GOOD && cmdTarget[pointer].badResp[i][0] != '\0')
                    {
                        if (strstr((const char*)sim800->tempBuffer, (const char*)&cmdTarget[pointer].badResp[i][0]) != NULL)
                        {
                            result = SIM800_RESPONSE_BAD;
                            break;
                        }
                    }
                }
                if (result == SIM800_RESPONSE_GOOD)
                {
                    SWTimer_Stop(&sim800->commandTimer);
                    sim800->statusResponse = 1;
                    sim800->rxWait = 0;
                    sim800->countAttempt = 0;
                    if (sim800->isNeedResponseValue)
                    {
                        checkResponseValue(sim800, sim800->tempBuffer);
                        sim800->isNeedResponseValue = 0;
                    }
                    (*sim800->discriptor.localState)++;
                    (*sim800->discriptor.cmdPointer)++;
                    DEBUG_sendString("pointer & state++\r\n");
                }
                else if (result == SIM800_RESPONSE_BAD)
                {
                    SWTimer_Stop(&sim800->commandTimer);
                    resendCmd(sim800);
                }
                
                if (sim800->state.receiveData == SIM_RCV_NOT)
                {
					clearTempBuffer(sim800);
                }
            }
        }
    }
}

/***************************Parse Strings**************************************/
void checkReceivePackets(SIM800_TypeDef* sim800)
{
	if (packetBuffer_getCount(&sim800->packetBuffer) != 0 &&
       (sim800->state.global == SIM800_SERVER_READY || sim800->state.global == SIM800_CLIENT_READY || sim800->state.global == SIM800_WAITING_MODE) &&
		rBuffer_getCount(&sim800->rxBuffer) == 0 &&
		sim800->state.sendData == SIM_SEND_IDLE && sim800->state.receiveData == SIM_RCV_NOT)
	{
		PacketInstance_t* pack = packetBuffer_pop(&sim800->packetBuffer);
		for (int i = 0; i < pack->countBytes; i++)
		{
			rBuffer_put(sim800->dataRxBuffer, pack->packet[i]);
		}
	}
}

void parseGprsRcvData(SIM800_TypeDef* sim800, uint8_t d)
{
	switch (sim800->state.receiveData)
	{
	case SIM_RCV_NOT: 	// find receive marker, +IPD
		if (strstr((const char*)sim800->tempBuffer, RECEIVE_DATA_MARKER) != NULL)
		{
			sim800->state.receiveData = SIM_RCV_MARKER;
			clearTempBuffer(sim800);
		}
		break;
	case SIM_RCV_MARKER: 	// find clause ','
		if (strstr((const char*)sim800->tempBuffer, ",") != NULL)
		{
			sim800->state.receiveData = SIM_RCV_SEP;
			clearTempBuffer(sim800);
		}
		break;
	case SIM_RCV_SEP: 	// find colon, ':'
		if (strstr((const char*)sim800->tempBuffer, ":") != NULL) {
			sim800->state.receiveData = SIM_RCV_PROCESS;
			uint8_t dBytes[10];
			uint16_t sz = rBuffer_getCount(&sim800->dataBytesBuffer);
			for (uint16_t i = 0; i < sz; i++)
			{
				dBytes[i] = rBuffer_pop(&sim800->dataBytesBuffer);
			}
			sim800->countDataBytes = atoi((const char*)dBytes);
			rBuffer_clear(&sim800->dataBytesBuffer);
			clearTempBuffer(sim800);
		}
		else
		{
			rBuffer_put(&sim800->dataBytesBuffer, d);
		}
		break;
	case SIM_RCV_PROCESS:
		if(sim800->curCounterDataBytes == 0)
		{
			packetBuffer_startPacket(&sim800->packetBuffer, packetBuffer_getPosition(&sim800->packetBuffer));
		}
		if (sim800->curCounterDataBytes < sim800->countDataBytes)
		{
			//rBuffer_put(sim800->dataRxBuffer, d);
			packetBuffer_putByte(&sim800->packetBuffer, d, packetBuffer_getPosition(&sim800->packetBuffer));
			//packetBuffer_put(&sim800->packetBuffer, &d, sim800->countDataBytes);
			sim800->curCounterDataBytes++;
		}
		if (sim800->curCounterDataBytes == sim800->countDataBytes)
		{
			if (sim800->callbacks.onSim800DataReceive)
			{
				sim800->callbacks.onSim800DataReceive(0, sim800->curCounterDataBytes);
			}
			packetBuffer_setPacket(&sim800->packetBuffer, sim800->curCounterDataBytes, packetBuffer_getPosition(&sim800->packetBuffer));
			sim800->state.receiveData = SIM_RCV_NOT;
			sim800->curCounterDataBytes = 0;
		}
		break;
	case SIM_RCV_FINISHED:
		break;
	default:
		break;
	}
}

uint8_t parseErrorReceive(SIM800_TypeDef* sim800)
{
	// sim card is OFF
	if (strstr((const char*)sim800->tempBuffer, SIMCARD_NOT_READY) != NULL && sim800->state.global > SIM800_READY)
	{
		SWTimer_Stop(&sim800->commandTimer);
		SIM800_ProgramReset(sim800);
		//sim800->state.global = SIM800_INIT;
		sim800->state.programInit = SIM800_PROGRAM_FAILED;
		return 1;
	}
	else if (strstr((const char*)sim800->tempBuffer, TCP_CON_CLOSED) != NULL && (sim800->state.global == SIM800_CLIENT_READY || sim800->state.global == SIM800_SERVER_READY))
	{
		SWTimer_Stop(&sim800->commandTimer);
		if (sim800->state.global == SIM800_CLIENT_READY)
		{
			SIM800_ClientReset(sim800);
			sim800->state.clientInit = SIM_CLIENT_FAILED;
		}
		return 1;
	}

	return 0;
}

void checkResponseValue(SIM800_TypeDef* sim800, uint8_t* data)
{
	switch (sim800->responseValueState)
	{
	case SIM_IP:
		uint8_t counterIp = 0;
		memset(sim800->ipAddr, 0, 20);
		while (*data != '\r')
		{
			sim800->ipAddr[counterIp++] = *data;
			data++;
		}
		break;
	case SIM_NET_QUALITY:
	{
		char *ptrCSQ  = strstr((const char*)sim800->tempBuffer, "CSQ");
		if (ptrCSQ != NULL)
		{
			char *ptrColon = strchr(ptrCSQ, ':');
			if (ptrColon != NULL)
			{
				char *ptrR = strchr(ptrCSQ, '\r');
				if (ptrR != NULL)
				{
					uint8_t csqData[10] = {0};
					uint8_t len = ptrR - ptrColon - 1;
					memcpy(csqData, ptrColon + 1, len);

					char *pointPtr = strchr(ptrCSQ, ',');
					if (pointPtr != NULL)
					{
						*pointPtr = '.';
					}
					sim800->netQuality = atof((char*)csqData);
				}
			}
		}
		break;
	}
	case SIM_BALANCE:
		uint8_t* ptrBegin = NULL;
		uint8_t* ptrDot = NULL;

        uint8_t* ptrUSSD = (uint8_t*)strstr(sim800->tempBuffer, "\"");

		for (uint16_t i = 0; i < sizeof(sim800->tempBuffer); i++)
		{
			if (isDigit(*(ptrUSSD + i)) && ptrBegin == NULL)
			{
				ptrBegin = ptrUSSD + i;
			}
#if defined BEELINE
			ptrDot = strchr(ptrBegin, '.');
#else if defined MTS
			ptrDot = strchr(ptrBegin, 'r');
#endif
			if (ptrBegin != NULL && ptrDot != NULL)
			{
 				break;
			}
		}

		if (ptrDot == NULL)
		{
			return;
		}
		uint8_t balanceData[20] = {0};
#if defined BEELINE
		uint8_t len = ptrDot - ptrBegin + 3;
#elif defined MTS
		uint8_t len = ptrDot - ptrBegin;
#endif
		memcpy(balanceData, ptrBegin, len);
		sim800->balance = atof((const char*)balanceData);
		break;
	}
}


/*************************Reset & clear Commons*******************************/
void SIM800_resetFlags(SIM800_TypeDef* sim800)
{
    sim800->rxWait = 0;
    sim800->statusResponse = 1;
    sim800->countAttempt = 0;
    clearTempBuffer(sim800);
    SENDDATA_reset();
    rBuffer_clear(&sim800->rxBuffer);
    rBuffer_clear(&sim800->txBuffer);
    rBuffer_clear(sim800->dataTxBuffer);
}

void clearTempBuffer(SIM800_TypeDef* sim800)
{
    sim800->tempBufferPointer = 0;
    memset(sim800->tempBuffer, '\0', sizeof(sim800->tempBuffer));
}

/***************************HW Callbacks**************************************/
void OnSerialRead(void* context, uint8_t data)
{
	SIM800_TypeDef* sim800 = (SIM800_TypeDef*)context;
	if (sim800)
	{
		rBuffer_put(&sim800->rxBuffer, data);
	}
}

/******************************Helpers****************************************/
uint8_t isDigit(uint8_t ch)
{
	return RANGE('0', ch, '9');
}

uint8_t countOfDigit(uint8_t n)
{
	return n ? 1 + countOfDigit(n / 10) : 0;
}